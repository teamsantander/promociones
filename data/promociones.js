var tarjetas = {

	 "Acumule más KMS. LATAM Pass con Rocketmiles.":{

	 	 "Nombre_tarjeta" : "Acumule más KMS. LATAM Pass con Rocketmiles",
		 "Bajada": "Exclusivo pagando con tu Tarjeta de Crédito Santander LATAM Pass.",
		 "a_href": "campanas/rocketmiles/",
		 "text_icon": "Beneficios",
		 "icon_tarjeta": "icon-Beneficios",
		 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
		 "link_externos" : "",
		 "Fecha_inicio":"16-03-2010",
		 "Fecha_fin":"18-03-2020",
		 "activado": "1",
		 "filtro": "latampass",
		 "img_caluga": "campanas/rocketmiles/img/rocketmiles-calugas-tc.jpg",
		 "Onclick": "ga('send','event','Tarjetas','Acumule más KMS. LATAM Pass con Rocketmiles.','Todas las Tarjetas');",
		 "url_out": "no"


	 },

	 "20% de descuento en Patagonia":{

	 	 "Nombre_tarjeta" : "20% de descuento en Patagonia",
		 "Bajada": "De lunes a viernes durante Agosto de 2017.",
		 "a_href": "https://mov.santander.cl/beneficios/products/patagonia",
		 "text_icon": "Beneficios",
		 "icon_tarjeta": "icon-Beneficios",
		 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
		 "link_externos" : "",
		 "Fecha_inicio":"16-03-2010",
		 "Fecha_fin":"18-03-2020",
		 "activado": "1",
		 "filtro": "otros",
		 "img_caluga": "campanas/patagonia/img/patagonia-calugas-tc.jpg",
		 "Onclick": "ga('send','pageview','/p41/20% de descuento en Patagonia');",
		 "url_out": "si"


	 },

	  "A Renueva tu auto.":{

	 	 "Nombre_tarjeta" : "Renueva tu auto",
		 "Bajada": " de 25 a 36 cuotas y acumula miles de kms. extras.",
		 "a_href": "campanas/renueva-tu-auto-alianza/index.asp",
		 "text_icon": "Tarjetas",
		 "icon_tarjeta": "icon_tarjeta",
		 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 "link_externos" : "",
		 "Fecha_inicio":"16-03-2010",
		 "Fecha_fin":"18-03-2020",
		 "activado": "1",
		 "filtro": "latampass",
		 "img_caluga": "campanas/renueva-tu-auto-bmw/img/remueve-tu-auto-caluga-tc.jpg",
		 "Onclick": "ga('send','event','Tarjetas',' Renueva tu auto.','Todas las Tarjetas');",
		 "url_out": "no"


	 },

	  "10 Cuotas":{
	 	 "Nombre_tarjeta" : "10 Cuotas",
	 	 "Bajada": "Compra todo lo que quieras en 10 cuotas con tasa preferencial con tus Tarjetas Santander.",
	 	 "a_href": "campanas/diez-cuotas/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/diez-cuotas/img/diez-cuotas-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Diez Cuotas ', 'calugas');",
	 	 "url_out": "no"

	 },

	  "Compra tus KMS. LATAM Pass con descuentos":{

	 	 "Nombre_tarjeta" : "Compra tus KMS. LATAM Pass con descuentos",
	 	 "Bajada": "Y también en 12 cuotas sin interés.",
	 	 "a_href": "campanas/compra-kms-lanpass/index.asp",
	 	 "text_icon": "LATAM Pass",
	 	 "icon_tarjeta": "icon-LATAM Pass",
	 	 "img_icon": "campanas/img/icons/ico-top-travel.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "latampass",
	 	 "img_caluga": "campanas/compra-kms-lanpass/img/compra-kms-lanpass-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Compra tus KMS. LATAM Pass con descuentos ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "¡Inscríbete y gana 10.000 kMS.!":{

		 	 "Nombre_tarjeta" : "¡Inscríbete y gana 10.000 kMS.!",
		 	 "Bajada": "Cumpliendo tu meta con tus Tarjetas Santander LATAM Pass.",
		 	 "a_href": "campanas/usala-y-gana-en-invierno-latampass/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "latampass",
		 	 "img_caluga": "campanas/usala-y-gana-en-invierno-latampass/img/usala-y-gana-en-invierno-latampass-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' Usala y Gana ', 'calugas');",
		 	 "url_out": "no"

	},

	"Los sábados multiplica X3 tus kms.":{

	 	 "Nombre_tarjeta" : "Los sábados multiplica X3 tus kms.",
	 	 "Bajada": "En Portales Cencosud pagando con tus Tarjetas de Crédito Santander LATAM Pass.",
	 	 "a_href": "campanas/multiplica-portal-cencosud/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "latampass",
	 	 "img_caluga": "campanas/multiplica-portal-cencosud/img/multiplica-portal-cencosud-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Los s&aacute;bados multiplica X3 tus kms. ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Disfruta La Parva con Santander":{

	 	 "Nombre_tarjeta" : "Disfruta La Parva con Santander",
	 	 "Bajada": "50% dcto. en ticket diario los miércoles y jueves.",
	 	 "a_href": "campanas/parva/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "img_caluga": "campanas/parva/img/parva-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' parva ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Compra una Ducati con tus Tarjetas de Crédito Santander LATAM Pass":{

		 	 "Nombre_tarjeta" : "Compra una Ducati con tus Tarjetas de Crédito Santander LATAM Pass",
		 	 "Bajada": "25 a 36 cuotas con tasa preferencia, además de un 3% dcto. en valor lista.",
		 	 "a_href": "campanas/venta-especial-ducati-select/index.asp",
		 	 "text_icon": "Descuentos",
		 	 "icon_tarjeta": "icon-Descuentos",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro":"latampass",
		 	 "img_caluga": "campanas/venta-especial-ducati-select/img/venta-especial-ducati-select-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas','Compra una Ducati con tus Tarjetas de Crédito Santander LATAM Pass','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	"Trattoria Rita":{

		 	 "Nombre_tarjeta" : "20% dcto. de lunes a jueves",
		 	 "Bajada": "En toda la carta en Trattoria Rita, ubicado en el boulevard del Parque Arauco.",
		 	 "a_href": "campanas/trattoria/index.asp",
		 	 "text_icon": "Beneficios",
		 	 "icon_tarjeta": "icon-Beneficios",
		 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/trattoria/img/trattoria-caluga-tc.jpg",
		 	 "Onclick": "ga('send','pageview','/p41/ 20% dcto. de lunes a jueves');",
		 	 "url_out": "no"

	},

	 "Cineplanet":{

	 	 "Nombre_tarjeta" : "Cineplanet",
	 	 "Url": "campanas/carga-tu-bip/index.asp",
	 	 "Bajada": "2x1 En salas 2D todos los jueves y viernes en Cineplanet.",
	 	 "a_href": "campanas/cineplanet/index.asp",
	 	 "class_img" :"icon-top",
	 	 "text_icon": "Beneficios",
	 	 "icon_tarjeta": "icon-Beneficios",
	 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/cineplanet/img/cineplanet-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Cineplanet ', 'calugas');",
	 	 "url_out": "no"

	 },


	"3 cuotas":{

		 	 "Nombre_tarjeta" : "3 cuotas",
		 	 "Bajada": "Compra todo en 3 cuotas sin interés con tus Tarjetas Santander.",
		 	 "a_href": "campanas/tres-cuotas/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/tres-cuotas/img/3coutas-caluga.png",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' Carga tu bip! con tus Tarjetas Santander ', 'calugas');",
		 	 "url_out": "no"

	},

	" Seguridad para tu Tarjeta de Débito":{

		 	 "Nombre_tarjeta" : "Seguridad para tu Tarjeta de Débito",
		 	 "Bajada": " Ahora podrás configurar el uso geográfico nacional e internacional de  tu Tarjeta de Débito.",
		 	 "a_href": "campanas/vgp/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjeta",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro":"",
		 	 "img_caluga": "campanas/vgp/img/vgp-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas',' Seguridad para tu Tarjeta de Débito','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	 "Starbucks":{

	 	 "Nombre_tarjeta" : "Starbucks: 30% dcto. sábados y domingos.",
	 	 "Bajada": "30% dcto. sábados y domingos en bebidas preparadas en barra con tus Tarjetas Santander.",
	 	 "a_href": "https://mov.santander.cl/beneficios/products/starbucks",
	 	 "text_icon": "Beneficios",
	 	 "icon_tarjeta": "icon-Beneficios",
	 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/descuento-starbucks/img/descuento-starbucks-caluga-tc.jpg",
	 	 "Onclick": "ga('send','pageview','/p41/Starbucks: 30% dcto. sábados y domingos.');",
	 	 "url_out": "si"

	 },

	  "Compra de KMS. LATAM Pass con Descuentos.":{

	 	 "Nombre_tarjeta" : "Compra de KMS. LATAM Pass con Descuentos.",
	 	 "Bajada": "Contratando un producto Santander.",
	 	 "a_href": "campanas/descuento_lanpass/index.asp",
	 	 "text_icon": "LATAM Pass",
	 	 "icon_tarjeta": "icon-LATAM Pass",
	 	 "img_icon": "campanas/img/icons/ico-top-travel.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "latampass",
	 	 "img_caluga": "campanas/descuento_lanpass/img/descuento_lanpass-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event','Tarjetas','Compra de KMS. LATAM Pass con Descuentos.','Todas las Tarjetas');",
	 	 "url_out": "no"

	 },

	  "Realiza tu Avance en Cuotas":{

	 	 "Nombre_tarjeta" : "Realiza tu Avance en Cuotas",
	 	 "Url": "campanas/avance-en-cuotas/index.asp",
	 	 "Bajada": "Como una transferencia inmediata desde tus Tarjetas de Crédito a tu cuenta.",
	 	 "a_href": "campanas/avance-en-cuotas/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/avance-en-cuotas/img/avance-en-cuotas-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Realiza tu Avance en Cuotas', 'calugas');",
	 	 "url_out": "no"

	 },

	  "15% dcto. en Le Due Torri":{

	 	 "Nombre_tarjeta" : "15% dcto. en Le Due Torri",
	 	 "Bajada": "Pagando con tus Tarjetas Santander.",
	 	 "a_href": "https://mov.santander.cl/beneficios/products/le-due-torri?segmento=s-personas",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/le-due-torri/img/le-due-torri-caluga-tc2.jpg",
	 	 "Onclick": "ga('send','pageview','/p41/15% dcto. en Le Due Torri');",
	 	 "url_out": "si"

	 },

	 "Pago Deuda Internacional":{

	 	 "Nombre_tarjeta" : "¿Necesitas pagar la deuda internacional de tu Tarjeta de Crédito?",
	 	 "Bajada": " Te damos las opciones que tienes para hacerlo. ",
	 	 "a_href": "campanas/pago-deuda-internacional/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "img_caluga": "campanas//pago-deuda-internacional/img/pago-deuda-internacional-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event','Tarjetas',' ¿Necesitas pagar la deuda internacional de tu Tarjeta de Crédito? ','Todas las Tarjetas');",
	 	 "url_out": "no"

	 },


	"Transferencia a terceros desde tus Tarjetas de Crédito":{

		 	 "Nombre_tarjeta" : "Transferencia a terceros desde tus Tarjetas de Crédito",
		 	 "Bajada": "A la cuenta que quieras y eliges en cuantas cuotas pagarlo. Aplica comisión e impuesto.",
		 	 "a_href": "campanas/transferencia-a-terceros/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjeta",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/transferencia-a-terceros/img/transferencia-a-terceros-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' Transferencia a terceros desde tus Tarjetas de Cr&eacute;dito ', 'calugas');",
		 	 "url_out": "no"

	},


	"Alianza Santander LATAM Pass - UC CHRISTUS.":{

		 	 "Nombre_tarjeta" : "Alianza Santander LATAM Pass - UC CHRISTUS.",
		 	 "Bajada": " Disfruta los beneficios de tus Tarjetas de Crédito Santander LATAM Pass. ",
		 	 "a_href": "campanas/ucchristus/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "latampass",
		 	 "img_caluga": "campanas/ucchristus/img/ucchristus-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas',' Alianza Santander LATAM Pass – UC CHRISTUS ','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	  "Renueva tu auto con tu Tarjeta de Crédito":{

	 	 "Nombre_tarjeta" : "Renueva tu auto con tu Tarjeta de Crédito",
	 	 "Bajada": "En cualquier automotora en 10 cuotas y a tasa preferencial.",
	 	 "a_href": "campanas/renueva-tu-auto-y-vuela-con-tus-tarjetas-de-credito/index.asp",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/renueva-tu-auto-y-vuela-con-tus-tarjetas-de-credito/img/head-auto-caluga.png",
	 	 "Onclick": "ga('send','event','Tarjetas','Renueva tu auto con tu Tarjeta de Crédito','Todas las Tarjetas');",
	 	 "url_out": "no"

	 },

	 "Cuotas Internacionales":{

	 	 "Nombre_tarjeta" : "Cuotas Internacionales",
	 	 "Bajada": "Transforma tu deuda internacional a pesos y en cuotas.",
	 	 "a_href": "campanas/cuotizacion/index.asp",
	 	 "alt": " Cuotizaci&oacute;n",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/cuotizacion/img/cuotizacion-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', 'Cuotizaci&oacute;n', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Conoce la tecnología Chip de tus Tarjetas Santander.":{

		 	 "Nombre_tarjeta" : "Conoce la tecnología Chip de tus Tarjetas Santander.",
		 	 "Bajada": "Las transacciones serán más seguras y confiables.",
		 	 "a_href": "campanas/tc_con-chip/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjeta",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/tc_con-chip/img/tc-con-chip-caluga.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas','Conoce la tecnología Chip de tus Tarjetas Santander.','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	 "Beneficios":{

	 	 "Nombre_tarjeta" : "Beneficios",
	 	 "Url": "campanas/avance-en-cuotas/index.asp",
	 	 "Bajada": "Conoce los Beneficios que tenemos para ti.",
	 	 "a_href": "campanas/beneficios/index.asp",
	 	 "class_img" :"icon-top",
	 	 "text_icon": "Beneficios",
	 	 "icon_tarjeta": "icon-Beneficios",
	 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/beneficios/img/beneficios-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Beneficios ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Activa tu clave PinPass AQUÍ.	":{

	 	 "Nombre_tarjeta" : "Activa tu clave PinPass AQUÍ",
		 "Bajada": "Activa tu clave PinPass y podrás disfrutar de realizar compras y avances.",
		 "a_href": "campanas/activa-tu-pinpass/index.asp",
		 "text_icon": "Tarjetas",
		 "icon_tarjeta": "icon_tarjeta",
		 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 "link_externos" : "",
		 "Fecha_inicio":"16-03-2010",
		 "Fecha_fin":"18-03-2020",
		 "activado": "1",
		 "filtro": "otros",
		 "img_caluga": "campanas/activa-tu-pinpass/img/activa-tu-pinpass-caluga-tc.jpg",
		 "Onclick": "ga('send','event', 'Tarjetas', ' Activa tu PinPass ', 'calugas');",
		 "url_out": "no"


	 },

	   "Carga tu bip! con tus Tarjetas Santander":{

	 	 "Nombre_tarjeta" : "Carga tu bip! con tus Tarjetas Santander",
	 	 "Url": "campanas/carga-tu-bip/index.asp",
	 	 "Bajada": "Valida el nuevo saldo de tu Tarjeta BIP en punto InfoBIP o tótem de Metro.",
	 	 "a_href": "campanas/carga-tu-bip/index.asp",
	 	 "class_img" :"icon-top",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/carga-tu-bip/img/carga-tu-bip-caluga-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Carga tu bip! con tus Tarjetas Santander ', 'calugas');",
	 	 "url_out": "no"

	 },


	 "SUPERPUNTOS":{

	 	 "Nombre_tarjeta" : "SUPERPUNTOS",
	 	 "Bajada": "Cientos de productos para comprar o canjear. Exclusivo para clientes Santander.",
	 	 "a_href": "campanas/super-catalogo/index.asp",
	 	 "text_icon": "Superpuntos",
	 	 "icon_tarjeta": "icon-Superpuntos",
	 	 "img_icon": "campanas/img/icons/ico-superpuntos.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "superpuntos",
	 	 "img_caluga": "campanas/super-catalogo/img/super-catalogo-caluga-tc.jpg",
	 	 "Onclick": "ga('send','pageview','/p41/Cientos de productos para comprar o canjear. Exclusivo para clientes Santander.');",
	 	 "url_out": "no"

	 },


	"Anticipo de KMS. LATAM Pass":{

		 	 "Nombre_tarjeta" : "Anticipo de KMS. LATAM Pass",
		 	 "Bajada": "Inscríbete aquí para adelantar kilómetros LATAM Pass",
		 	 "a_href": "campanas/anticipolanpass/index.asp",
		 	 "text_icon": "LATAM Pass",
		 	 "icon_tarjeta": "icon-LATAM Pass",
		 	 "img_icon": "campanas/img/icons/ico-top-travel.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "latampass",
		 	 "img_caluga": "campanas/anticipolanpass/img/anticipolanpass-calugas-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas','Anticipo de KMS. LATAM Pass','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	"25 o más cuotas.":{

		 	 "Nombre_tarjeta" : "25 o más cuotas.",
		 	 "Bajada": "Compra todo lo que quieras de 25 a 36 cuotas a una tasa preferencial de 0,99%.",
		 	 "a_href": "campanas/veinticinco_o_mas_cuotas_099/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/veinticinco_o_mas_cuotas_099/img/header-caluga.png",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' Usala y Gana ', 'calugas');",
		 	 "url_out": "no"

	},

	"Multiplica tus KMS. LATAM Pass ":{

		 	 "Nombre_tarjeta" : " Multiplica tus KMS. LATAM Pass ",
		 	 "Bajada": " Conoce las tiendas adheridas.",
		 	 "a_href": "campanas/oferta-kms-lanpass/index.asp",
		 	 "text_icon": "LATAM Pass",
		 	 "icon_tarjeta": "icon-LATAM Pass",
		 	 "img_icon": "campanas/img/icons/ico-top-travel.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro":"latampass",
		 	 "img_caluga": "campanas/multiplica_kilometros/img/oferta-kms-lanpass-calugas-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas',' Multiplica tus KMS. LATAM Pass ','Todas las Tarjetas');",
		 	 "url_out": "no"

	},

	"Descuentos Restaurantes":{

	 	 "Nombre_tarjeta" : "Dscto. Restaurantes",
	 	 "Bajada": "30% de dcto. en todos los restaurantes asociados.",
	 	 "a_href": "campanas/clubdelectores_restaurantes/index.asp",
	 	 "class_img" :"icon-top",
	 	 "text_icon": "Beneficios",
	 	 "icon_tarjeta": "icon-Beneficios",
	 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro" : "clublectores",
	 	 "img_caluga": "campanas/clubdelectores_restaurantes/img/clubdelectores_restaurantes-tc.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Dscto. Restaurantes ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Conoce las Ventas Especiales. ":{

		 	 "Nombre_tarjeta" : "Conoce las Ventas Especiales",
		 	 "Bajada": "De tus Tarjeta Club de Lectores American Express.",
		 	 "a_href": "campanas/venta-especial-club-de-lectores/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro" : "clublectores",
		 	 "img_caluga": "campanas/venta-especial-club-de-lectores/img/venta-especial-club-de-lectores-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' Venta especial Club de Lectores. ', 'calugas');",
		 	 "url_out": "no"

	},
	
		"Beneficios exclusivos para ti":{

		 	 "Nombre_tarjeta" : "Beneficios exclusivos para ti",
		 	 "Bajada": "Aprovecha estos beneficios con tu Tarjeta Club de Lectores American Express.",
		 	 "a_href": "campanas/beneficios-exclusivos-club-de-lectores/index.asp",
		 	 "text_icon": "Beneficios",
		 	 "icon_tarjeta": "icon-Beneficios",
		 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/beneficios/img/disfruta-beneficios-exclusivos-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event','Tarjetas','Beneficios exclusivos para ti','Todas las Tarjetas');",
		 	 "url_out": "no"

	},




	 "Activación internacional":{

	 	 "Nombre_tarjeta" : "Activación internacional",
	 	 "a_href": "campanas/atm_internacional/index.asp",
	 	 "Bajada": "Activa tu Tarjeta de Débito para usarla en el extranjero.",
	 	 "text_icon": "Tarjetas",
	 	 "icon_tarjeta": "icon_tarjetas",
	 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
	 	 "link_externos" : "",
	 	 "Fecha_inicio":"16-03-2010",
	 	 "Fecha_fin":"18-03-2020",
	 	 "activado": "1",
	 	 "filtro": "otros",
	 	 "img_caluga": "campanas/atm_internacional/img/atm-internacional-caluga.jpg",
	 	 "Onclick": "ga('send','event', 'Tarjetas', ' Activa tu tarjeta de d&eacute;bito para usarla en el  extranjero ', 'calugas');",
	 	 "url_out": "no"

	 },

	 "Vive con tu Redcompra de Banco Santander":{

		 	 "Nombre_tarjeta" : "Vive con tu Redcompra de Banco Santander",
		 	 "Bajada": "Redcompra es andar con tu dinero, pero mucho más seguro.",
		 	 "a_href": "campanas/tarjeta_redcompra/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjeta",
		 	 "img_icon": "campanas/img/icons/ico-top-cards.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/tarjeta_redcompra/img/tarjeta_redcompra-caluga-tc.jpg",
		 	 "Onclick": "ga('send','event', 'Tarjetas', ' COMPRA TODO LO QUE QUIERAS DE 25 A 36 CUOTAS ', 'calugas');",
		 	 "url_out": "no"

	},

	"25 o más cuotas Select.":{

		 	 "Nombre_tarjeta" : "25 o más cuotas.",
		 	 "Bajada": "Compra todo lo que quieras de 25 a 36 cuotas a una tasa preferencial de 0,69%.",
		 	 "a_href": "campanas/veinticinco_o_mas_cuotas_069_select/index.asp",
		 	 "text_icon": "Tarjetas",
		 	 "icon_tarjeta": "icon_tarjetas",
		 	 "img_icon": "campanas/img/icons/ico-top-rojo-01.png",
		 	 "link_externos" : "",
		 	 "Fecha_inicio":"16-03-2010",
		 	 "Fecha_fin":"18-03-2020",
		 	 "activado": "1",
		 	 "filtro": "otros",
		 	 "img_caluga": "campanas/veinticinco_o_mas_cuotas_069_select/img/head-caluga.png",
		 	 "Onclick": "ga('send','event','Tarjetas','25 o más cuotas.','Todas las Tarjetas');",
		 	 "url_out": "no"

	}

}