<!--#include virtual="/tarjetas/inc_files/new_header.asp" -->
<style> #filtroContainer{display: none;} .secondary{margin-bottom: 40px;}</style>
<section class="banner banner-home">
    <div id="banner" role="toolbar">
        <section class="bannerSlider">  
            <div class="itemSlider" data-name="La Parva" data-slick-index="0" aria-hidden="true" role="option">
                <a href="/tarjetas/campanas/parva/index.asp" class="linkBanner">
                	<img src="/tarjetas/campanas/parva/img/parva-banner-hometc.jpg" alt="Ir a La Parva">
                </a>
                <div class="infoBanner">
                    <div class="contSlideInfo">
                        <a href="/tarjetas/campanas/parva/index.asp" class="infoSlide">
                            <span class="barraLoading"></span>
                            <h3>Disfruta La Parva con Santander</h3>
                            <h2>50% dcto. en ticket diario los miércoles y jueves.</h2>
                            <p></p>
                            <span class="vermas">Ver más</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="itemSlider" data-name="Patagonia">
                <a href="https://mov.santander.cl/beneficios/products/patagonia" class="linkBanner">
                	<img src="/tarjetas/campanas/patagonia/img/patagonia-banner-tc.jpg" alt="Ir a Patagonia">
                </a>
                <div class="infoBanner">
                    <div class="contSlideInfo">
                        <a href="https://mov.santander.cl/beneficios/products/patagonia" class="infoSlide">
                            <span class="barraLoading"></span>
                            <h3>20% DE DESCUENTO EN PATAGONIA</h3>
                            <h2>De lunes a viernes durante Agosto</h2>
                            <p></p>
                            <span class="vermas">Ver más</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="itemSlider" data-name="Tres cuotas">
                <a href="/tarjetas/campanas/tres-cuotas/index.asp" class="linkBanner">
                	<img src="/tarjetas/campanas/tres-cuotas/img/tres-cuotas-banner-tc.jpg" alt="Ir a tres-cuotas">
                </a>
                <div class="infoBanner">
                    <div class="contSlideInfo">
                        <a href="/tarjetas/campanas/tres-cuotas/index.asp" class="infoSlide">
                            <span class="barraLoading"></span>
                            <h3>3 CUOTAS</h3>
                            <h2>Compra todo en 3 cuotas sin interés con tus tarjetas Santander.</h2>
                            <p></p>
                            <span class="vermas">Ver más</span>
                        </a>
                    </div>
                </div>
            </div> 
        </section>
        <section class="dotsBanner">
        </section>
    </div>
</section>
<div id="sliderContainer" class="container space-card"></div>
<main class="page-home"> 
	<section>
		<div class="container">
			<div class="cont_calugas 2">  
				<div class="row clearfix" id="calugas"> 
					<div class="col-md-3 col-sm-6">
					   <div class="caluga">
					      <div class="thumbs">
					         <a class="linkPromo" href="" title="" onclick="ga('send','event', 'Tarjetas', ' Renueva tu auto', 'calugas');">
					         	<img src="" alt="" class="icon-top iconImg"><span class="text-icon">Tarjetas</span>
					         	<img class="imgCard" src="" alt="Renueva tu auto" >
					         </a>
					      </div>
					      <div class="info eq-height">
					         <h2 class="h3">
					         	<a class="linkPromo" href="" title=" Renueva tu auto" onclick="ga('send','event', 'Tarjetas', ' Renueva tu auto', 'calugas');"> Renueva tu auto </a>
					         </h2>
					         <p >
					         	<a class="linkPromo" href="" title=" Renueva tu auto" onclick="ga('send','event', 'Tarjetas', ' Renueva tu auto', 'calugas');">de 25 a 36 cuotas y acumula miles de kms. extras.</a>
					         </p>
					         <div class="text-right">
					            <div class="text-right">
					            	<a href="" class="to-the-right see-more linkPromo" onclick="ga('send','event', 'Tarjetas', ' Renueva tu auto', 'calugas');">Ver más</a>
					            </div>
					         </div>
					      </div>
					   </div>
					</div>
					<div class="col-md-3 col-sm-6">
					   <div class="caluga">
					      <div class="thumbs"> 
					         	<a class="linkPromo" href="" title="" onclick="ga('send','event', 'Tarjetas', 'Starbucks', 'calugas');">
					         	<img src="" alt="" class="icon-top iconImg">
					         	<span class="text-icon">Beneficios</span>
					         	<img class="imgCard" src="" alt="Starbucks">
					         </a>
					      </div>
					      <div class="info eq-height">
					         <h2 class="h3">
					         	<a class="linkPromo" href="" title="Starbucks" onclick="ga('send','event', 'Tarjetas', 'Starbucks', 'calugas');">Starbucks</a>
					         </h2>
					         <p>
					         	<a class="linkPromo" href="" title="Starbucks" onclick="ga('send','event', 'Tarjetas', 'Starbucks', 'calugas');">30% dcto. sábados y domingos en bebidas preparadas en barra con tus Tarjetas Santander.</a>
					         </p>
					         <div class="text-right">
					            <div class="text-right">
					            	<a class="to-the-right see-more linkPromo" href="" onclick="ga('send','event', 'Tarjetas', 'Starbucks', 'calugas');">Ver más</a>
					            </div>
					         </div>
					      </div>
					   </div>
					</div>
					<div class="col-md-3 col-sm-6">
					   <div class="caluga">
					      <div class="thumbs">
					         	<a class="linkPromo" href="" title="" onclick="ga('send','event', 'Tarjetas', 'Cuotizaci&oacute;n', 'calugas');"> 
					         		<img src="" alt="" class="icon-top iconImg">
					         		<span class="text-icon">Tarjetas</span>
					         		<img class="imgCard" src="" alt="Cuotizaci&oacute;n">
					         	</a>
					      </div>
					      <div class="info eq-height">
					         <h2 class="h3">
					         	<a class="linkPromo" href="" title="Cuotizaci&oacute;n" onclick="ga('send','event', 'Tarjetas', 'Cuotizaci&oacute;n', 'calugas');">Cuotas Internacionales</a>
					         </h2>
					         <p>
					         	<a class="linkPromo" href="" title="Starbucks" onclick="ga('send','event', 'Tarjetas', 'Cuotizaci&oacute;n', 'calugas');">Transforma tu deuda internacional a pesos, y págalo en cuotas</a>
					         </p>
					         <div class="text-right">
					            <div class="text-right">
					            	<a href="" class="to-the-right see-more linkPromo" onclick="ga('send','event', 'Tarjetas', 'Cuotizaci&oacute;n', 'calugas');">Ver más</a>
					            </div>
					         </div>
					      </div>
					   </div>
					</div>  
					<div class="col-md-3 col-sm-6">
						<div class="caluga">
							<div class="thumbs">
								<a class="linkPromo" href="" title="ir a Cineplanet" onclick="ga('send','event', 'Tarjetas', 'Cineplanet', 'calugas');">
									<img src="" alt="Cineplanet" class="icon-top iconImg"><span class="text-icon">Beneficios</span>
									<img class="imgCard" src="" alt="Cineplanet">
								</a>
							</div>
							<div class="info eq-height">
								<h2 class="h3">
									<a class="linkPromo" href="" title="ir a  Cineplanet" onclick="ga('send','event', 'Tarjetas', ' Cineplanet ', 'calugas');">Cineplanet </a>
								</h2>
								<p>
									<a class="linkPromo" href="" title="ir a  Cineplanet " onclick="ga('send','event', 'Tarjetas', ' Cineplanet ', 'calugas');"> 2x1 En salas 2D todos los jueves y viernes en Cineplanet </a>
								</p>
								<div class="text-right">
									<div class="text-right">
										<a href="" class="to-the-right see-more linkPromo" onclick="ga('send','event', 'Tarjetas', ' Cineplanet ', 'calugas');">Ver más</a>
									</div>
								</div>
							</div>
						</div>
					</div> 
				</div>  
			</div> 
			<div class="row">
				<div class="col-md-12 text-center">
					<a id="allPromo" href="" class="btn-default secondary">Ver todas las promociones</a>
				</div>
			</div>
		</div>
	</section> 
	
	<section id="filtroContainer">
		<div class="container">
			<h2 class="text-left">¿No sabes cuál es la Tarjeta ideal para ti?</h2>
			<div class="card">
				<div class="row">
					<div class="col-md-12">
						<h3 class="bordered-title"> Buscar tarjetas según mis ingresos </h3>
						<p>Selecciona el rango de tus ingresos mensuales</p>
					</div> 
					<div class="col-lg-7 col-lg-offset-1">
						<div class="rangeBox">
							<form >
							    <div class="range">
									<input id="inputRange" name="range" type="range" min="1" max="13" onchange="changeInput()">
									<div class="range-output">
										<output class="output" name="output" for="range" id="rangeText">
										$400.000 a $699.999
										</output>
									</div>
								</div>
							</form>
						</div>
					</div>
	                <div class="col-lg-2 col-lg-offset-1">    
						<button class="btn btn-default mar-top-30" id="search">Buscar</button> 
					</div>
				</div> 
			</div>  
		</div>  
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-6 text-left">
					<h2>Nuestros Programas</h2>
					<div class="card">
						<div class="eq-square">
							<div id="programas" class="to-left">
								<ul>
									<li><a href="/tarjetas/tarjetas-santander/nuestras-tarjetas/#lanpass" title="LATAM Pass"><img src="/tarjetas/assets/img/home/logo-latam-pass.jpg" alt="LANTAM Pass"></a></li>
									<li><a href="/tarjetas/tarjetas-santander/nuestras-tarjetas/clubdelectores/index.asp" title="Club de Lectores"><img src="/tarjetas/assets/img/home/logo-club-de-lectores.jpg" alt="Club de Lectores el Mercurio"></a></li>
									<li><a href="/tarjetas/tarjetas-santander/nuestras-tarjetas/#cocha" title="Cocha"><img src="/tarjetas/assets/img/home/logo-cocha.jpg" alt="Cocha"></a></li>
									<li><a href="/tarjetas/tarjetas-santander/nuestras-tarjetas/superpuntos/index.asp" title="Súper Puntos"><img src="/tarjetas/assets/img/home/logo-superpuntos.jpg" alt="Superpuntos"></a></li>
									<li><a href="/tarjetas/tarjetas-santander/nuestras-tarjetas/santander321/index.asp" title="Santander 321"><img src="/tarjetas/assets/img/home/logo-321.jpg" alt="Santander 3 2 1"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 text-left">
					<h2>¿Cómo revisar mi estado de cuenta?</h2>
					<div class="card">
						<div class="eq-square">
							<p>Te ayudamos a revisar correctamente el estado de cuenta de tu tarjeta</p>
							<div class="img-cuenta text-center">
								<a href="../../../informacion/eecc/index.asp" target="_blank"><i class="icon-lupa"></i></a>
								<img src="/tarjetas/assets/img/img-boleta.jpg" alt="Boleta">
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</section>
	<section id="filtro">
		<div class="container">
			<h2>¿Tienes alguna duda sobre nuestras tarjetas?</h2> 
			<div class="card">
				<div class="row"> 
					<div class="col-md-6">
						<h3>Aún no tengo tarjetas Santander</h3>
						<ul class="list-more">
							<li><a href="preguntas-frecuentes/index.asp?id=one" class="see-more">¿Qué es una tarjeta de Crédito y cómo funciona?</a> </li>
							<li><a href="preguntas-frecuentes/index.asp?id=two" class="see-more">¿Cuáles son las ventajas de las Tarjetas de Crédito Santander?</a></li>
							<li><a href="preguntas-frecuentes/index.asp?id=three" class="see-more">¿Cuáles son los requisitos básicos para tener una Tarjeta de Crédito?</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<h3>Ya tengo mi tarjeta Santander</h3>
						<ul class="list-more">
							<li><a href="preguntas-frecuentes/index.asp?id=four" class="see-more">¿Cuándo puedo empezar a usar mi tarjeta?</a></li>
							<li><a href="preguntas-frecuentes/index.asp?id=five" class="see-more">¿Cómo activo mi PinPass?</a></li>
							<li><a href="preguntas-frecuentes/index.asp?id=six" class="see-more">¿Qué debo hacer si se extravía o roban mi Tarjeta de Crédito, Débito o SuperClave?</a></li>
						</ul>
					</div> 					
				</div>
				<div class="row">
					<div class="col-md-12 text-center"> 
						<a href="/tarjetas/tarjetas-santander/preguntas-frecuentes/index.asp" class="btn-default">Ver todas las preguntas</a> 
					</div>
				</div>
			</div> 
		</div>
	</section>
</main>
<script src="/data/collections_tarjetas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>

<!-- #include virtual="/tarjetas/inc_files/footer.asp"-->