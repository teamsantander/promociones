<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output indent="yes" method="html" omit-xml-declaration="yes"/>
<xsl:template match="/">
<section class="banner banner-home">
	<div id="banner">
	  <section class="bannerSlider">
		<xsl:for-each select="banners/banner">
			<div class="itemSlider" data-name="{nombre}">
			<a href="{url_destino}" class="linkBanner"><img src="{imagen}" alt="Ir a {nombre}" /></a> 	
			<div class="infoBanner">
			  <div class="contSlideInfo">
				  <a href="{url_destino}" class="infoSlide">
					  <span class="barraLoading"></span>
					  <h3><xsl:value-of select="subtitulo" /></h3>
					  <h2><xsl:value-of select="titulo" /></h2>
					  <p></p>
					  <span class="vermas">Ver más</span>
				  </a>
			  </div>
			</div>
		</div>
		</xsl:for-each>
	  </section>
	<section class="dotsBanner"></section> 
	</div>
</section>
</xsl:template>
</xsl:stylesheet>


