<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output indent="yes" method="html" omit-xml-declaration="yes"/>
<xsl:template match="/">
<h3>Ingresa tus datos</h3>
<form action="{content/formulario/@action}" method="{content/formulario/@method}" class="{content/formulario/@class}">
	<div class="field">
		<label>Rut</label>
		<input type="text" class="rut-input" placeholder="Ej: 11.111.111-1" />
	</div>
	<div class="field">
		<label>Clave</label>
		<input type="password" class="password-input" placeholder="4 dígitos" maxlength="4" />
	</div>
	<div class="field">
		<input type="submit" value="Enviar" />
	</div>
</form>
</xsl:template>
</xsl:stylesheet>
