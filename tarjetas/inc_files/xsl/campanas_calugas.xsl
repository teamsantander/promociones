<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output indent="yes" method="html" omit-xml-declaration="yes"  />
<xsl:param name="urlRelativa" />
<xsl:param name="tipo" />
<xsl:param name="fecha" />
<xsl:param name="filtro" />
<xsl:template match="/">
<section class="">
	<xsl:attribute name="class">
		<xsl:choose>
			<xsl:when test="$tipo='caluga'">container</xsl:when>
            <xsl:when test="$tipo='calugas'">container</xsl:when>
			<xsl:when test="$tipo='all'">inner-container</xsl:when>
		</xsl:choose>
	</xsl:attribute>
	<h2 class="subtitle home-subtitle"></h2>


		<div class="cont_calugas 2" data-current="{$fecha}">
			<div class="row clearfix" id="calugas">

					<xsl:choose>
						<xsl:when test="$tipo = 'caluga'">
							<xsl:for-each select="campanas/campana">
							 <xsl:sort select="@destacado" order="descending" />
							<xsl:if test="@destacado >= 0">
								<div class="" data-inicio="{fecha-inicio}" data-fin="{fecha-fin}">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="$tipo = 'caluga'">contenedor-caluga col-md-3 col-sm-6</xsl:when>
											<xsl:when test="$tipo = 'all'">contenedor-caluga col-sm-4</xsl:when>
											<xsl:otherwise>contenedor-caluga col-md-3 col-sm-6</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<div class="caluga" >
										<div class="thumbs">
											<a href="{$urlRelativa}{url}" title="ir a " onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');" style="overflow:hidden">
												<img src="{$urlRelativa}img/icons/{icon}" alt="" class="icon-top" />
												<span class="text-icon"><xsl:value-of select="icon/@title" disable-output-escaping="yes" /></span>
												<img src="{$urlRelativa}{img-caluga}" alt="{nombre}" style="height:132px; width:auto;" />
												<xsl:if test="extra-info">
														<span class="tag bg-color-{extra-info/@clase}"><xsl:value-of select="extra-info" disable-output-escaping="yes" /></span>
												</xsl:if>
											</a>
										</div>
										<div class="info eq-height">
											<h2 class="h3"><a href="{$urlRelativa}{url}" title="ir a {nombre}" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');"><xsl:value-of select="titulo-caluga" disable-output-escaping="yes" /></a></h2>
											<p style="vertical-align:none"> <a href="{$urlRelativa}{url}" title="ir a {nombre}" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');"><xsl:value-of select="bajada-caluga" disable-output-escaping="yes" /></a></p>
                                            <div class="text-right">
                                            <div class="text-right">
											<a href="{$urlRelativa}{url}" class="to-the-right see-more" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');">Ver Más</a>
                                            </div>
                                            </div>
										</div>

									</div>
								</div>

							</xsl:if>
							</xsl:for-each>
						</xsl:when>


						<xsl:otherwise>
							<xsl:for-each select="campanas/campana">
                            <xsl:if test="@destacado >= 0">
                            <xsl:if test="$filtro = @etiqueta or not($filtro)">
								<div class="" data-inicio="{fecha-inicio}" data-fin="{fecha-fin}">
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="$tipo = 'caluga'">contenedor-caluga col-md-3 col-sm-6</xsl:when>
											<xsl:when test="$tipo = 'all'">contenedor-caluga col-sm-4</xsl:when>
											<xsl:otherwise>contenedor-caluga col-md-3 col-sm-6</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<div class="caluga">
										<div class="thumbs">
											<a href="{$urlRelativa}{url}" title="ir a " onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');" style="overflow:hidden">
												<img src="{$urlRelativa}img/icons/{icon}" alt="" class="icon-top" />
												<span class="text-icon icon-{icon/@title}"><xsl:value-of select="icon/@title" disable-output-escaping="yes" /></span>
												<img src="{$urlRelativa}{img-caluga}" alt="{nombre}" style="height:132px; width:auto;" />
												<xsl:if test="extra-info">
														<span class="tag bg-color-{extra-info/@clase}"><xsl:value-of select="extra-info" disable-output-escaping="yes" /></span>
												</xsl:if>

											</a>
										</div>
										<div class="info eq-height">
											<h2 class="h3"><a href="{$urlRelativa}{url}" title="ir a {nombre}" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');"><xsl:value-of select="titulo-caluga" disable-output-escaping="yes" /></a></h2>
											<p style="vertical-align:none"> <a href="{$urlRelativa}{url}" title="ir a {nombre}" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');"><xsl:value-of select="bajada-caluga" disable-output-escaping="yes" /></a></p>
                                            <div class="text-right">
                                            <div class="text-right">
											<a href="{$urlRelativa}{url}" class="to-the-right see-more" onclick="ga('send','event', 'Tarjetas', '{nombre}', 'calugas');">Ver Más</a>
                                            </div>
                                            </div>
										</div>

									</div>
								</div>
                                </xsl:if>
                                </xsl:if>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>



			</div>

		</div>
		<a data-tipo="{$tipo}" href="campanas/todas.asp" class="to-the-right see-more mar-right-10 only-home mar-bottom-20">Ver todas las promociones</a>
</section >
<xsl:if test="$tipo='caluga'">
<script>
$( document ).ready(function() {
	$("#calugas").children().each(function(i, elem){
		if(i>3){
			elem.remove();
		}
	})
});
</script>
</xsl:if>
<script>
	$(function(){
		$(".contenedor-caluga").each(function(){
			var inicio = toDate($(this).data("inicio"));
			var fin = new Date(toDate($(this).data("fin")).getTime() + (24*60*60*1000));
			var cur = toDate($(".cont_calugas.2").data("current"));
			if(inicio>cur || cur>=(fin)){
				$(this).remove();
			}
		});
	});
</script>
</xsl:template>
</xsl:stylesheet>
