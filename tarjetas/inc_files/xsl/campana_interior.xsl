<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output indent="yes" method="html" omit-xml-declaration="yes"/>
<xsl:param name="id" />
<xsl:param name="servidor" />
<xsl:output encoding="UTF-8" />
<xsl:param name="urlRelativa" />
<xsl:param name="urlAbsoluta" />
<xsl:template match="/">

<div class="breadcrumbs white-bg">
			<div class="container">
				<p><a href="/tarjetas/tarjetas/"><span>Tarjetas</span></a><i>/</i><a href="/tarjetas/tarjetas/campanas/todas.asp"><span>Promociones</span></a><i>/</i><a class="current" href="javascript:void(0)" title="Enlace a "><xsl:value-of select="campanas/campana[@id=$id]/titulo-caluga" disable-output-escaping="yes" /></a></p>
			</div>
		</div>
<main class="container">
	
	<div class="tit-filtro">
		<h2><xsl:value-of select="campanas/campana[@id=$id]/titulo-caluga" disable-output-escaping="yes" /></h2>
	</div>

	<section class="inner-container">

	<div class="row">
		<xsl:choose>
			<xsl:when test="campanas/campana[@id=$id]/img-banner/@tipo = 1">
				<div class="cover-descuento left-info">
					<div class="desc-info">
						<div class="v-align">
							<div class="core">
								<div class="brackets {campanas/campana[@id=$id]/contenido/titulo-brackets/@class}">
									<span></span><span></span><span></span><span></span>
									<h3><xsl:value-of select="campanas/campana[@id=$id]/contenido/titulo-brackets" disable-output-escaping="yes" /></h3>
								</div>
								<xsl:if test="campanas/campana[@id=$id]/contenido/subtitulo-brackets">
									<p>
										<xsl:value-of select="campanas/campana[@id=$id]/contenido/subtitulo-brackets" disable-output-escaping="yes"/>
									</p>
								</xsl:if>

							</div>
						</div>
					</div>
					<div class="desc-img full">
						<img src="{$urlRelativa}{campanas/campana[@id=$id]/img-banner}" alt="{campanas/campana[@id=$id]/nombre}"/>
					</div>
				</div>
			</xsl:when>

			<xsl:when test="campanas/campana[@id=$id]/img-banner/@tipo = 2">
				<div class="cover-descuento right-info">
					<div class="desc-info">
						<div class="v-align">
							<div class="core">
								<div class="brackets {campanas/campana[@id=$id]/contenido/titulo-brackets/@class}">
									<span></span><span></span><span></span><span></span>
									<h3><xsl:value-of select="campanas/campana[@id=$id]/contenido/titulo-brackets" disable-output-escaping="yes" /></h3>
								</div>
								<xsl:if test="campanas/campana[@id=$id]/contenido/subtitulo-brackets">
									<p>
										<xsl:value-of select="campanas/campana[@id=$id]/contenido/subtitulo-brackets" disable-output-escaping="yes"/>
									</p>
								</xsl:if>

							</div>
						</div>
					</div>
					<div class="desc-img full">
						<img src="{$urlRelativa}{campanas/campana[@id=$id]/img-banner}" alt="{campanas/campana[@id=$id]/nombre}"/>
					</div>
				</div>
			</xsl:when>

			<xsl:when test="campanas/campana[@id=$id]/img-banner/@tipo = 3">
				<div class="cover-descuento cent-info">
					<div class="desc-info">
						<div class="v-align">
							<div class="core">
								<div class="brackets {campanas/campana[@id=$id]/contenido/titulo-brackets/@class}">
									<span></span><span></span><span></span><span></span>
									<h3><xsl:value-of select="campanas/campana[@id=$id]/contenido/titulo-brackets" disable-output-escaping="yes" /></h3>
								</div>
								<xsl:if test="campanas/campana[@id=$id]/contenido/subtitulo-brackets">
									<p>
										<xsl:value-of select="campanas/campana[@id=$id]/contenido/subtitulo-brackets" disable-output-escaping="yes"/>
									</p>
								</xsl:if>

							</div>
						</div>
					</div>
					<div class="desc-img full">
						<img src="{$urlRelativa}{campanas/campana[@id=$id]/img-banner}" alt="{campanas/campana[@id=$id]/nombre}"/>
					</div>
				</div>
			</xsl:when>
			<xsl:when test="campanas/campana[@id=$id]/img-banner/@tipo = 4">
				<div class="cover-promo">
						<div class="promo-img">
							<img src="{$urlRelativa}{campanas/campana[@id=$id]/img-banner}" alt="{campanas/campana[@id=$id]/nombre}"/>
						</div>
						<div class="promo-info">
							<div class="v-cent">
								<div class="core">

									<div class="brackets {campanas/campana[@id=$id]/contenido/titulo-brackets/@class}">
										<span></span><span></span><span></span><span></span>
										<h3><xsl:value-of select="campanas/campana[@id=$id]/contenido/titulo-brackets" disable-output-escaping="yes" /></h3>
									</div>
									<p><xsl:value-of select="campanas/campana[@id=$id]/contenido/subtitulo-brackets" disable-output-escaping="yes" /></p>
								</div>
							</div>
						</div>
					</div>

			</xsl:when>
			<xsl:when test="campanas/campana[@id=$id]/img-banner/@tipo = 5">
				<div class="cover-promo left-info">
						<div class="promo-img">
							<img src="{$urlRelativa}{campanas/campana[@id=$id]/img-banner}" alt="{campanas/campana[@id=$id]/nombre}"/>
						</div>
						<div class="promo-info">
							<div class="v-cent">
								<div class="core">

									<div class="brackets {campanas/campana[@id=$id]/contenido/titulo-brackets/@class}">
										<span></span><span></span><span></span><span></span>
										<h3><xsl:value-of select="campanas/campana[@id=$id]/contenido/titulo-brackets" disable-output-escaping="yes" /></h3>
									</div>
									<p><xsl:value-of select="campanas/campana[@id=$id]/contenido/subtitulo-brackets" disable-output-escaping="yes" /></p>
								</div>
							</div>
						</div>
					</div>

			</xsl:when>
		</xsl:choose>

		</div>

		<div class="row campana_row">

			<div class="box inner">
			<xsl:attribute name="class">box inner <xsl:if test="(campanas/campana[@id=$id]/contenido/container/@class) and (campanas/campana[@id=$id]/contenido/container/@class != '')"><xsl:value-of select="campanas/campana[@id=$id]/contenido/container/@class" /></xsl:if></xsl:attribute>


			<xsl:if test="campanas/campana[@id=$id]/contenido/tabs">

					<ul class="tabs">
						<xsl:for-each select="campanas/campana[@id=$id]/contenido/tabs/tab">
							<xsl:choose>
								<xsl:when test="position() = 1">
									<li class="active"><a href="#tab{@id}"><xsl:value-of select="@titulo" /></a></li>
								</xsl:when>
								<xsl:otherwise>
									<li><a href="#tab{@id}"><xsl:value-of select="@titulo" /></a></li>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</ul>
					<section class="inner-container">
						<div class="cont_calugas">
							<div class="row">
								<div class="tab_container bordered-bottom ohidden pad-top-30 pad-bottom-30">
									<xsl:for-each select="campanas/campana[@id=$id]/contenido/tabs/tab">
										<div id="tab{@id}" class="tab_content">
											<xsl:for-each select="col">
												<div class="col-md-{@cols} col-xs-12">
													<h3 class="color-red"><xsl:value-of select="@titulo" /></h3>
													<xsl:value-of select="content" disable-output-escaping="yes" />
													<xsl:if test="content/formulario/@activado">
														<h3>Ingresa tus datos</h3>
														<form action="{content/formulario/@serv}/transa/cruce.asp" method="{content/formulario/@method}" class="{content/formulario/@class}">
															<div class="field">
																<label>Rut</label>
																<input type="text" class="rut-input" placeholder="Ej: 11.111.111-1" />
															</div>
															<div class="field">
																<label>Clave</label>
																<input type="password" class="password-input" placeholder="4 dígitos" maxlength="4" />
															</div>
															<div class="field">
																<!--<input type="submit" value="Enviar" />-->
																<input class="btn-red auto-cent-768" type="submit" value="Enviar" />
															</div>
														</form>
													</xsl:if>
												</div>
											</xsl:for-each>
										</div>
									</xsl:for-each>
								</div>
							</div>
						</div>
					</section>
				</xsl:if>
				<xsl:if test="campanas/campana[@id=$id]/contenido/container and campanas/campana[@id=$id]/contenido/container != ''">
					<section class="container">
						<div class="full ohidden mar-bottom-30">
							<div class="row">
								<div class="cont_calugas">
									<xsl:if test="campanas/campana[@id=$id]/contenido/container/contenido-container and campanas/campana[@id=$id]/contenido/container/contenido-container != ''">
										<xsl:for-each select="campanas/campana[@id=$id]/contenido/container/contenido-container/col">
											<div class="col-md-{@cols} col-xs-12">
												<h3 class="color-red"><xsl:value-of select="@titulo" /></h3>
												<xsl:value-of select="content" disable-output-escaping="yes" />
												<xsl:if test="content/formulario/@activado">
													<h3 class="color-red" id="titulo-rut-clave">Ingresa tus datos</h3>
                                                    <xsl:choose>
                                                    <xsl:when test="content/formulario/@tipo = 'superventa'">
													<form action="{$servidor}/transa/cruceSuperVenta.asp" method="{content/formulario/@method}" class="{content/formulario/@class}" onsubmit="javascript:return verificarRutGeneral(document.autent.d_rut.value,0,1);"  name="autent" id="formulario_int_camp">
                                                    	<input type="hidden" value="BancoSantander" name="IDLOGIN" />
               											<input type="hidden" value="" name="Op_campana" />
                                                        <input name="codigosuperventa" value="{content/formulario/@op}" type="hidden" />
                                                        <div class="cajaInput">
                                                            <input  type="text" class="input cajarut" maxlength="12" name="d_rut" required="required" />
                                                            <span class="barra"></span>
                                                            <label class="label" for="">Rut</label>
                                                        </div>
                                                        <div class="cajaInput">
                                                            <input type="password" class="input cajaclave" size="4" maxlength="4" name="d_pin" required="required" />
                                                            <span class="barra"></span>
                                                            <label class="label" for="">Clave:</label>
                                                            <input type="hidden" name="tipo" />
															<input type="hidden" name="pin" />
                                                        </div>
												<div class="field">
                                                   <xsl:choose>
                                                    <xsl:when test="content/formulario/@value = 1">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Inscríbete aquí" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 2">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Hazlo aquí" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 3">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Activa tu clave" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 4">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Ingresar" />													
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Enviar" />
                                                    </xsl:otherwise>
       												</xsl:choose>
												</div>
													</form>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                    <form action="{$servidor}/transa/cruce.asp" method="{content/formulario/@method}" class="{content/formulario/@class}" onsubmit="javascript:return verificarRutGeneral(document.autent.d_rut.value,0,1);"  name="autent" id="formulario_int_camp">
                                                    	<input type="hidden" value="BancoSantander" name="IDLOGIN" />
               											<input type="hidden" value="{content/formulario/@op}" name="Op_campana" />
                                                        <div class="cajaInput">
                                                            <input  type="text" class="input cajarut" maxlength="12" name="d_rut" required="required" />
                                                            <span class="barra"></span>
                                                            <label class="label" for="">Rut</label>
                                                            <input type="hidden" size="12" maxlength="12" name="rut" />
                                                        </div>
                                                        <div class="cajaInput">
                                                            <input type="password" class="input cajaclave" size="4" maxlength="4" name="d_pin" required="required" />
                                                            <span class="barra"></span>
                                                            <label class="label" for="">Clave:</label>
                                                            <input type="hidden" name="tipo" />
															<input type="hidden" name="pin" />
                                                        </div>
												<div class="field">
                                                   <xsl:choose>
                                                    <xsl:when test="content/formulario/@value = 1">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Inscríbete aquí" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 2">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Hazlo aquí" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 3">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Activa tu clave" />													
                                                    </xsl:when>
                                                    <xsl:when test="content/formulario/@value = 4">
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Ingresar" />													
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                    <input id="cubrex" class="botonNew botonNewSmall botonPrimario" type="submit" value="Enviar" />
                                                    </xsl:otherwise>
       												</xsl:choose>
												</div>
													</form>
                                                    </xsl:otherwise>
       												</xsl:choose>
                                                    	
												</xsl:if>
											</div>
										</xsl:for-each>
									</xsl:if>
								</div>
							</div>
						</div>
					</section>
				</xsl:if>

			<a href="#" class="link red auto-cent" title="Ver más" data-show-target="legal"><span>Condiciones y restricciones</span> <i class="icon-element small-down-arrow-icon"></i></a>

			<div class="hidden-target mar-top-40 ohidden legal-campanas" data-show="legal"><xsl:value-of select="campanas/campana[@id=$id]/contenido/legal" disable-output-escaping="yes"/></div>
			<div class="oval-feed mar-bottom-40">
				<p>Comparte esta promoción</p>
				<ul>
					<li><a href="https://twitter.com/share" class="twitter-share-button" data-size="small"></a></li>
					<li><div class="fb-share-button" data-href="{$urlAbsoluta}{campanas/campana[@id=$id]/url}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={$urlAbsoluta}{campanas/campana[@id=$id]/url}">Compartir</a></div></li>
					<!--<li><a href="https://www.youtube.com/user/SantanderChile" class="" title="Enlace a Youtube oficial de Santander"><i class="youtube"></i></a></li>-->
				</ul>
			</div>
		</div>

	</div>

	</section>
</main>
<xsl:if test="campanas/campana[@id=$id]/link-solicitud and campanas/campana[@id=$id]/link-solicitud != '' ">
<section class="full ohidden white-bg mar-bottom-40">
			<div class="container mar-top-30 mar-bottom-10">
				<div class="row">
					<div class="col-sm-12 col-md-6 tarjeta-solicitud">
                    		<xsl:value-of select="campanas/campana[@id=$id]/link-solicitud/imagen" disable-output-escaping="yes" />
					</div>
					<div class="col-sm-12 col-md-6">
						<h2><xsl:value-of select="campanas/campana[@id=$id]/link-solicitud/titulo" disable-output-escaping="yes" /></h2>
						<p><a href="{campanas/campana[@id=$id]/link-solicitud/url}" class="link red" title="ir a tarjetas" onclick="{campanas/campana[@id=$id]/link-solicitud/onclick}"><xsl:value-of select="campanas/campana[@id=$id]/link-solicitud/bajada" disable-output-escaping="yes" /></a></p>
					</div>
				</div>
			</div>
		</section>

</xsl:if>
<script>
		$(function(){
				var titulo = "<xsl:value-of select='campanas/campana[@id=$id]/titulo-caluga' disable-output-escaping='yes' />";
				var txt = document.createElement("textarea");
    			txt.innerHTML = titulo;
				var titulo = txt.value;
				document.title = "Santander Tarjetas - " + titulo;
		})
</script>
</xsl:template>
</xsl:stylesheet>
