<!-- #include virtual="/tarjetas/inc_files/header.asp" -->
<style>
	.block{
		display: block !important; 
	}
	.block2 {
		display: none!important;
	}
</style>
<main id="preguntas-frecuentes" class="container">
	<section id="info-top" class="col-md-12 container-up no-gutter">
	  <div class="col-md-12">
	    <div class="encabezado">
	      <h1 id="tarjet">Preguntas frecuentes</h1>
	    </div>
	  </div>
	</section>
	<ul class="tabs only-text margin-cero">
	  <li class="active"><a href="#tab1">Todas las preguntas</a></li>
	  <li class=""><a href="#tab2">Aún no tengo mi tarjeta</a></li>
	  <li class=""><a href="#tab3">Ya tengo mi tarjeta</a></li>
	</ul>
	
	<div class="tab_container ohidden pad-bottom-40">

	  <div id="tab1" class="tab_content">
	    <div class="accordion">
	      <article id="p1">
	        <h4>¿Qué es una tarjeta de Crédito y cómo funciona?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Es un medio de pago y un instrumento de crédito o financiamiento personal e intransferible, que permite al cliente adquirir bienes y servicios en cualquier establecimiento comercial del país y el extranjero.</p>
	            <p>El banco, previa evaluación de datos del cliente (rentas e informes comerciales) le otorga montos en dinero (pesos y/o dólares) a la tarjeta, los cuales el cliente puede utilizar para comprar, pagar y girar, tanto nacionalmente como internacionalmente.</p>
	          </div>
	        </div>
	      </article>
	      <article id="p2">
	        <h4>¿Cuáles son las ventajas de las Tarjetas de Crédito Santander?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Permite hasta 38 días de crédito sin intereses. Entre la fecha de la compra y su pago (fecha de vencimiento del Estado de Cuenta) pueden transcurrir hasta 38 días por lo que permiten comprar o pagar sin necesidad de tener el dinero en ese
	              mismo momento. Si la deuda se paga completamente en la fecha de vencimiento se accede a financiamiento sin intereses de la compra durante el plazo antes indicado. Si la deuda es cancelada después de la fecha de vencimiento del Estado de
	              Cuenta, genera intereses cuya tasa se informa mes a mes en el Estado de Cuenta.</p>


	            <ul>
	              <li>Es un medio de pago abierto y válido en cualquier comercio que acepte Tarjeta de Crédito en Chile o el extranjero.</li>
	              <li>Habilitada para realizar avances en efectivo desde cajeros automáticos, en Chile y el extranjero.</li>
	              <li>Solicitud de Tarjetas Adicionales sin costo para el titular.</li>
	              <li>Permite al cliente seleccionar entre diferentes fechas de pago.</li>
	              <li>Puede decidir el monto del pago cada mes, optando por pagar entre el mínimo fijado por el banco o el total de la deuda del Estado de Cuenta.</li>
	              <li>Acceso al servicio de Pago Automático de Cuentas de Servicios con Tarjeta de Crédito (PAT).</li>
	              <li>Tienen los mejores programas de acumulación por compras. KMS. LANPASS, Puntos ClubMovistar, SuperPuntos, descuentos y más. Revisa el detalle de cada una de las tarjetas en la sección "nuestras tarjetas"</li>
	            </ul>
	          </div>
	        </div>
	      </article>
	      <article id="p3">
	        <h4>¿Cuáles son los requisitos básicos para tener una Tarjeta de Crédito?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <ul>
	              <li>Nacionalidad Chilena o Extranjera con residencia definitiva en el país</li>
	              <li>Edad mínima 21 años</li>
	              <li>Solicitud de Tarjetas Adicionales sin costo para el titular.</li>
	              <li>Ingresos mensuales superiores a $400.000 (Clientes nuevos).</li>
	              <li>Buenos antecedentes comerciales (aprobación sujeta a política comercial y de riesgo vigente).</li>
	            </ul>
	          </div>
	        </div>
	      </article>
	      <article id="p4">
	        <h4>¿Cuándo puedo empezar a usar mi tarjeta?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Después de <strong>24 horas hábiles</strong> de recibir su tarjeta, active su PinPass, la clave secreta de Tarjeta de Crédito para cajeros automáticos, que también sirve para comprar en comercios.</p>
	            <p>Una vez activada su clave PinPass, por Internet a través de Santander.cl podrá realizar compras y giros en cajeros automáticos al día hábil siguiente. Si activa su clave PinPass a través de cajeros automáticos podrá realizar compras en comercios
	              de forma inmediata y avances al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	          </div>
	        </div>
	      </article>
	      <article  id="p5">
	        <h4>¿Cómo activo mi PinPass?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p><strong>A través de Cajeros Automáticos:</strong></p>
	            <ol>
	              <li>1) Ingrese su Tarjeta de Crédito en el Cajero Automático y digite los 4 primeros dígitos de su RUT.</li>
	              <li>2) Luego seleccione la opción "Activación de PinPass".</li>
	              <li>3) Luego le solicitará ingresar su actual número secreto (4 primeros dígitos de su Rut). Para posteriormente, ingresar su nueva clave y confirmarla.</li>
	            </ol>
	            <p>Una vez activado el PinPass de su Tarjeta podrá realizar compras en comercios de forma inmediata y avances al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	            <p>Estos pasos deben repetirse para la activación de cada una de sus nuevas Tarjetas de Crédito.</p>
	            <p><strong>A través de Internet:</strong></p>
	            <ol>
	              <li>1) Ingrese a Santander.cl, digite su rut con su clave de acceso, seleccione "Tarjetas de Crédito" y luego "PinPass".</li>
	              <li>2) Seleccione las Tarjetas para cambio de clave. Ingrese y confirme su nuevo número secreto (clave de 4 dígitos que usted escoja).</li>
	              <li>3) Confirme operación con su tarjeta Súper Clave. Si no la tiene, solicítela en cualquier sucursal Santander.</li>
	            </ol>
	            <p>Una vez activado el PinPass de su Tarjeta podrá realizar compras y giros al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	          </div>
	        </div>
	      </article>
	      <article  id="p6">
	        <h4>¿Qué debo hacer si olvido mi clave PinPass?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Debes llamar a <strong>Vox (600) 320 3000</strong>, pedir reseteo de la Clave y luego ir a cualquier Cajero Automático para crear una nueva clave.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué debo hacer si se extravía o roban mi Tarjeta de Crédito, Débito o SuperClave?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Debes bloquear inmediatamente tus Tarjetas llamando a <strong>Vox al (600) 320 3000(servicio disponible las 24 horas)</strong> o bien dirigirse al Mesón de Atención de Clientes de la sucursal más cercana.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué es el Pago Automático en Tarjeta de Crédito (PATPASS)?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Es un servicio que permite suscribir los pagos de cuentas como la luz, agua, gas, etc. en su Tarjeta de Crédito. Lo que le permite puntualidad en el pago de cuentas (sin atrasos); ahorro de Tiempo (pago de cuentas sin hacer filas, sin trámites;
	              y orden (todas las cuentas de Servicio en un solo Estado de Cuenta, con 1 fecha de pago). Adicionalmente, acumula KMS. LANPASS, Puntos ClubMovistar, Superpuntos o el programa al cual pertenezca la tarjeta. </p>

	            <p>Las cuentas se pueden suscribir , eliminar, o modificar a través de <strong>Santander.cl</strong>, <strong>Vox (600 320 3000)</strong> o con su <strong>ejecutivo de cuentas</strong>. Más información en www.PATPASS.cl.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿A quienes le puedo otorgar una Tarjeta Adicional y cuántas puedo tener?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Puede otorgar tarjetas adicionales a cualquier persona mayor de 14 años. No existe un límite de tarjetas adicionales.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué PinPass debe utilizar la Tarjeta de crédito adicional adicional?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Los adicionales deben hacer el mismo proceso de obtención de clave PinPass que un cliente titular.</p>
	          </div>
	        </div>
	      </article>
	    </div>
	  </div>

	  <div id="tab2" class="tab_content">
	    <div class="accordion">
	      <article>
	        <h4>¿Qué es una tarjeta de Crédito y cómo funciona?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Es un medio de pago y un instrumento de crédito o financiamiento personal e intransferible, que permite al cliente adquirir bienes y servicios en cualquier establecimiento comercial del país y el extranjero.</p>
	            <p>El banco, previa evaluación de datos del cliente (rentas e informes comerciales) le otorga montos en dinero (pesos y/o dólares) a la tarjeta, los cuales el cliente puede utilizar para comprar, pagar y girar, tanto nacionalmente como internacionalmente.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Cuáles son las ventajas de las Tarjetas de Crédito Santander?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Permite hasta 38 días de crédito sin intereses. Entre la fecha de la compra y su pago (fecha de vencimiento del Estado de Cuenta) pueden transcurrir hasta 38 días por lo que permiten comprar o pagar sin necesidad de tener el dinero en ese
	              mismo momento. Si la deuda se paga completamente en la fecha de vencimiento se accede a financiamiento sin intereses de la compra durante el plazo antes indicado. Si la deuda es cancelada después de la fecha de vencimiento del Estado de
	              Cuenta, genera intereses cuya tasa se informa mes a mes en el Estado de Cuenta.</p>


	            <ul>
	              <li>Es un medio de pago abierto y válido en cualquier comercio que acepte Tarjeta de Crédito en Chile o el extranjero.</li>
	              <li>Habilitada para realizar avances en efectivo desde cajeros automáticos, en Chile y el extranjero.</li>
	              <li>Solicitud de Tarjetas Adicionales sin costo para el titular.</li>
	              <li>Permite al cliente seleccionar entre diferentes fechas de pago.</li>
	              <li>Puede decidir el monto del pago cada mes, optando por pagar entre el mínimo fijado por el banco o el total de la deuda del Estado de Cuenta.</li>
	              <li>Acceso al servicio de Pago Automático de Cuentas de Servicios con Tarjeta de Crédito (PAT).</li>
	              <li>Tienen los mejores programas de acumulación por compras. KMS. LANPASS, Puntos ClubMovistar, SuperPuntos, descuentos y más. Revisa el detalle de cada una de las tarjetas en la sección "nuestras tarjetas"</li>
	            </ul>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Cuáles son los requisitos básicos para tener una Tarjeta de Crédito?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <ul>
	              <li>Nacionalidad Chilena o Extranjera con residencia definitiva en el país</li>
	              <li>Edad mínima 21 años</li>
	              <li>Solicitud de Tarjetas Adicionales sin costo para el titular.</li>
	              <li>Ingresos mensuales superiores a $400.000 (Clientes nuevos).</li>
	              <li>Buenos antecedentes comerciales (aprobación sujeta a política comercial y de riesgo vigente).</li>
	            </ul>
	          </div>
	        </div>
	      </article>
	    </div>
	  </div>

	  <div id="tab3" class="tab_content">
	    <div class="accordion">
	      <article>
	        <h4>¿Cuándo puedo empezar a usar mi tarjeta?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Después de <strong>24 horas hábiles</strong> de recibir su tarjeta, active su PinPass, la clave secreta de Tarjeta de Crédito para cajeros automáticos, que también sirve para comprar en comercios.</p>
	            <p>Una vez activada su clave PinPass, por Internet a través de Santander.cl podrá realizar compras y giros en cajeros automáticos al día hábil siguiente. Si activa su clave PinPass a través de cajeros automáticos podrá realizar compras en comercios
	              de forma inmediata y avances al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Cómo activo mi PinPass?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p><strong>A través de Cajeros Automáticos:</strong></p>
	            <ol>
	              <li>1) Ingrese su Tarjeta de Crédito en el Cajero Automático y digite los 4 primeros dígitos de su RUT.</li>
	              <li>2) Luego seleccione la opción "Activación de PinPass".</li>
	              <li>3) Luego le solicitará ingresar su actual número secreto (4 primeros dígitos de su Rut). Para posteriormente, ingresar su nueva clave y confirmarla.</li>
	            </ol>
	            <p>Una vez activado el PinPass de su Tarjeta podrá realizar compras en comercios de forma inmediata y avances al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	            <p>Estos pasos deben repetirse para la activación de cada una de sus nuevas Tarjetas de Crédito.</p>
	            <p><strong>A través de Internet:</strong></p>
	            <ol>
	              <li>1) Ingrese a Santander.cl, digite su rut con su clave de acceso, seleccione "Tarjetas de Crédito" y luego "PinPass".</li>
	              <li>2) Seleccione las Tarjetas para cambio de clave. Ingrese y confirme su nuevo número secreto (clave de 4 dígitos que usted escoja).</li>
	              <li>3) Confirme operación con su tarjeta Súper Clave. Si no la tiene, solicítela en cualquier sucursal Santander.</li>
	            </ol>
	            <p>Una vez activado el PinPass de su Tarjeta podrá realizar compras y giros al dia hábil siguiente. Si la activación/cambio se realiza un fin de semana o festivo, su tarjeta estará operativa al dia hábil siguiente.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué debo hacer si olvido mi clave PinPass?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Debes llamar a <strong>Vox (600) 320 3000</strong>, pedir reseteo de la Clave y luego ir a cualquier Cajero Automático para crear una nueva clave.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué debo hacer si se extravía o roban mi Tarjeta de Crédito, Débito o SuperClave?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Debes bloquear inmediatamente tus Tarjetas llamando a <strong>Vox al (600) 320 3000(servicio disponible las 24 horas)</strong> o bien dirigirse al Mesón de Atención de Clientes de la sucursal más cercana.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué es el Pago Automático en Tarjeta de Crédito (PATPASS)?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Es un servicio que permite suscribir los pagos de cuentas como la luz, agua, gas, etc. en su Tarjeta de Crédito. Lo que le permite puntualidad en el pago de cuentas (sin atrasos); ahorro de Tiempo (pago de cuentas sin hacer filas, sin trámites;
	              y orden (todas las cuentas de Servicio en un solo Estado de Cuenta, con 1 fecha de pago). Adicionalmente, acumula KMS. LANPASS, Puntos ClubMovistar, Superpuntos o el programa al cual pertenezca la tarjeta. </p>

	            <p>Las cuentas se pueden suscribir , eliminar, o modificar a través de <strong>Santander.cl</strong>, <strong>Vox (600 320 3000)</strong> o con su <strong>ejecutivo de cuentas</strong>. Más información en www.PATPASS.cl.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿A quienes le puedo otorgar una Tarjeta Adicional y cuántas puedo tener?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Puede otorgar tarjetas adicionales a cualquier persona mayor de 14 años. No existe un límite de tarjetas adicionales.</p>
	          </div>
	        </div>
	      </article>
	      <article>
	        <h4>¿Qué PinPass debe utilizar la Tarjeta de crédito adicional adicional?<span class="icon-flecha-abajo flecha"></span></h4>
	        <div class="block">
	          <div class="contenedorAcordeon">
	            <p>Los adicionales deben hacer el mismo proceso de obtención de clave PinPass que un cliente titular.</p>
	          </div>
	        </div>
	      </article>
	    </div>
	  </div>

	</div>
</main>

<script src="/data/collections_tarjetas.js"></script>
<!-- #include virtual="/tarjetas/inc_files/footer.asp" -->