$( document ).ready(function() {
    setTitle("pfrecuentes");
    function acordeon(){
        $(".block .contenedorAcordeon").hide();
        $(".accordion h4").on("click",function(){
            var is_active = false;
            if($(this).find("span.icon-flecha-abajo").hasClass("flechaActiva")){
                is_active = true;
            }
            $(".accordion h4 span.icon-flecha-abajo").removeClass("flechaActiva");
            if(!is_active){
                $(this).find("span.icon-flecha-abajo").addClass("flechaActiva");
            }
            if($(this).next().find(".contenedorAcordeon").is(":visible")){
                $(this).next().find(".contenedorAcordeon").slideUp();
            }
            if($(this).next().find(".contenedorAcordeon").is(":hidden")){
                $(".accordion h4").next().find(".contenedorAcordeon").slideUp();
                $(this).next().find(".contenedorAcordeon").slideDown();
            }
        });
    }
    acordeon();
    var nameTab =  document.getElementsByClassName("data-text");
    nameTab[0].innerText = "Menú";
});

$(window).load(function(){
    openSite();
})

function setTitle(name){

    var data = tituloBanco.filter(item => { return item.name === name})
    var nuestrasTarjetas = $("title");
    nuestrasTarjetas.text(data[0].value);    

};


function openSite(){
    let url = window.location.href;
    let id = url.split('=');

    switch (id[1]){
        case "one":
            let a = "p1";
            desplazar($("#p1"));
            abrirAcordeon(a);
            break;
        case "two":
            let b = "p2";
            desplazar($("#p2"));
            abrirAcordeon(b);
            break;
        case "three":
            let c = "p3";
            desplazar($("#p3"));
            abrirAcordeon(c);
            break;
        case "four":
            let d = "p4";
            desplazar($("#p4"));
            abrirAcordeon(d);
            break;
        case "five":
            let e = "p5";
            desplazar($("#p5"));
            abrirAcordeon(e);
            break;
        case "six":
            let f = "p6";
            desplazar($("#p6"));
            abrirAcordeon(f);
            break;
        default:
            //console.log("fail");
    }
}

function desplazar(ubicacion){
    var posicion = ubicacion.offset().top;
    //event.preventDefault()
    $("html, body").animate({
        scrollTop: posicion
    }, 2000);
};

function abrirAcordeon(acordeon){
    let url = "#" + acordeon + " .block .contenedorAcordeon";
    document.querySelector(url).setAttribute("style","display:block");
    document.querySelector(url).classList.add("flechaActiva");
}
