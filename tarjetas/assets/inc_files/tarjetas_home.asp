			<section class="cont-inner mar-top-10">
				<div class="slider-tarjetas mar-bottom-30">
 <%
	Set objXMLDoc = Server.CreateObject("Microsoft.XMLDOM")
	objXMLDoc.async = False
	objXMLDoc.load(Server.MapPath("\") & "\tarjetas\tarjetas\inc_files\xml\tarjetas.xml")

	if objXMLDoc.parseError.errorcode <> 0 then ' Función que analiza gramaticalmente el objeto XML en busca de algún error
		Response.Write("XML Error…") 'Si encuentra error, lo comunica...
	end if

	Set ini = objXMLDoc.documentElement
	Set NodeList = objXMLDoc.getElementsByTagName("tarjeta")

	dim Limite
	Limite= 4



	For each nodeTarjeta in NodeList
		If contador > Limite Then
			Exit For
		End If
		Set nombre = nodeTarjeta.getElementsByTagName("nombre")(0)
		Set imagen = nodeTarjeta.getElementsByTagName("imagen")(0)
		Set descripcion = nodeTarjeta.getElementsByTagName("descripcion")(0)
		Set solicitar = nodeTarjeta.getElementsByTagName("solicitar")(0)
		Set url = nodeTarjeta.getElementsByTagName("url")(0)
		Set ingreso = nodeTarjeta.getElementsByTagName("ingreso")(0)
		Set tipoTarjeta = nodeTarjeta.getElementsByTagName("tipo")(0)
		Set mantencion = nodeTarjeta.getElementsByTagName("cmantencion")(0)
%>


					<div class="slider-item">
							<div class="box inner tarjeta">
							<h3 class="color-red"><%=nombre.text%></h3>

							<div class="row">
								<div class="col-md-3 col-sm-3">
									<div class="img">
										<img class="tiptip" title="Viajes" src="../tarjetas/img/icons/icon_viaje.png">
										<img src="<%=imagen.text%>" alt='<%=imagen.getAttribute("alt")%>'>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<ul class="list">
										<%
											Set nodeListas = nodeTarjeta.getElementsByTagName("listas/lista")
											For each list in nodeListas
										%>
										<li><%=list.childNodes(0).text%></li>

										<% Next %>
									</ul>
								</div>

								<div class="col-md-3 col-sm-3">
									<div class="relacionada">
												<p><strong><%= nodeTarjeta.getElementsByTagName("cmantencion")(0).getAttribute("title") %></strong></p>
												<p><%=mantencion.text %></p>
										<div class="center-element text-center">
										<% If solicitar.text <> "" Then %>
											<a href="<%=solicitar.text%>" class="btn-red"> Solicitar</a>
											<a href="<%=url.text%>" class="see-more animated cent-auto">Ver Más</a>
										<% Else %>
											<a href="<%=url.text%>" class="see-more animated cent-auto"> Ver más</a>
										<% End If%>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>





<%
	contador = contador +1
	Next
%>

				</div>
			</section>
