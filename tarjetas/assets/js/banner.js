var itemBannerSlider = $(".itemSlider");

itemBannerSlider.each(function( index ) {
	var bgImgItemSlider = $(this).find("img").attr("src");
	$(this).find(".linkBanner").css("background-image","url(" + bgImgItemSlider + ")");
});

var time = 4;
var $bar,
	$slick,
	isPause,
	tick,
	percentTime;

$slick = $(".banner-home .bannerSlider");

$(".banner-home .bannerSlider").slick({
	autoplay: true,
	appendDots: $(".dotsBanner"),
	arrows:false,
	dots: true,
	infinite: true,
	autoplaySpeed: 4500,
	slidesToShow: 1,
	fade: true,
	slidesToScroll: 1,
	customPaging : function(slider, i) {
	  var thumb = $(slider.$slides[i]).data("name");
	  return "<span>"+thumb+"</span>";
  },
  responsive: [{
	  breakpoint: 600,
	  settings: {
		  fade: false
	  }
  }]
});

if ($(".slider-tarjetas").length) {
	$(".slider-tarjetas").slick({
		autoplay: true,
		arrows:false,
		dots: true,
		infinite: true,
		autoplaySpeed: 4500,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [{
		  breakpoint: 600,
		  settings: {
			  fade: false
		  }
	  }]
	});
};

$bar = $(".banner-home .barraLoading");

$(".banner-home #banner").on({
mouseenter: function() {
  isPause = true;
},
mouseleave: function() {
  isPause = false;
}
});

function startProgressbar(){
	resetProgressbar();
	percentTime = 0;
	isPause = false;
	tick = setInterval(interval, 10);
};

function interval() {
	if(isPause === false) {
		percentTime += 1 / (time + 0.1);
		$bar.css({
			width: percentTime + "%"
		});
		if(percentTime >= 100){
			$slick.slick("slickNext");
			startProgressbar();
		};
	};
};

function resetProgressbar() {
	$bar.css({
	 	width: 0+"%"
	});
	clearTimeout(tick);
}

if ($(".bannerSlider").length) {
	startProgressbar();
};