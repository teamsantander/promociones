$( document ).ready(function() {
    setTitle("home");
    });

var id;
// funciones al cargar el sitio
window.onload = function(){
	var url_base = localStorage.getItem("url_dinamica");
	let ball = document.getElementsByClassName("rangeslider-fill-lower");
	let bar = document.getElementsByClassName("rangeslider-thumb");
	/*setTimeout(function(){
		ball[0].style.width = "0px";
		bar[0].style.left = "0px";
	},1);*/

	var infoSlide = document.getElementsByClassName("infoSlide");
	var promo = document.getElementsByClassName("linkPromo");
	var allPromo = document.getElementById("allPromo");
	var logos = document.querySelectorAll("#programas ul li a");

	/*logos[0].setAttribute('href', url_base + 'nuestras-tarjetas/index.asp?id=' + 'lanpass');
	logos[1].setAttribute('href', url_base + 'nuestras-tarjetas/index.asp?id=' + 'clublectores');
	logos[2].setAttribute('href', url_base + 'nuestras-tarjetas/index.asp?id=' + 'cocha');
	logos[3].setAttribute('href', url_base + 'nuestras-tarjetas/index.asp?id=' + 'superpuntos');
	logos[4].setAttribute('href', url_base + 'nuestras-tarjetas/index.asp?id=' + '321');*/
	logos[0].setAttribute("href", "https://www.santander.cl/tarjetas/personas/nuestras-tarjetas/lanpass/index.asp");
	logos[1].setAttribute("href", "https://www.santander.cl/tarjetas/personas/nuestras-tarjetas/clubdelectores/index.asp");
	logos[2].setAttribute("href", "https://www.santander.cl/tarjetas/personas/nuestras-tarjetas/cocha/index.asp");
	logos[3].setAttribute("href", "https://www.santander.cl/tarjetas/personas/nuestras-tarjetas/superpuntos/index.asp");
	logos[4].setAttribute("href", "https://www.santander.cl/tarjetas/personas/nuestras-tarjetas/santander321/index.asp");

	allPromo.setAttribute('href', url_base + "campanas/index.asp");

	promo[0].setAttribute("href", url_base + "campanas/renueva-tu-auto-alianza/index.asp");
	promo[1].setAttribute("href", url_base + "campanas/renueva-tu-auto-alianza/index.asp");
	promo[2].setAttribute("href", url_base + "campanas/renueva-tu-auto-alianza/index.asp");
	promo[3].setAttribute("href", url_base + "campanas/renueva-tu-auto-alianza/index.asp");

	promo[4].setAttribute("href", url_base + "campanas/descuento-starbucks/index.asp");
	promo[5].setAttribute("href", url_base + "campanas/descuento-starbucks/index.asp");
	promo[6].setAttribute("href", url_base + "campanas/descuento-starbucks/index.asp");
	promo[7].setAttribute("href", url_base + "campanas/descuento-starbucks/index.asp");
	
	promo[8].setAttribute("href", url_base + "campanas/cuotizacion/index.asp");
	promo[9].setAttribute("href", url_base + "campanas/cuotizacion/index.asp");
	promo[10].setAttribute("href", url_base + "campanas/cuotizacion/index.asp");
	promo[11].setAttribute("href", url_base + "campanas/cuotizacion/index.asp");

	promo[12].setAttribute("href", url_base + "campanas/cineplanet/index.asp");
	promo[13].setAttribute("href", url_base + "campanas/cineplanet/index.asp");
	promo[14].setAttribute("href", url_base + "campanas/cineplanet/index.asp");
	promo[15].setAttribute("href", url_base + "campanas/cineplanet/index.asp");

	var iconImg = document.getElementsByClassName("iconImg");

	iconImg[0].setAttribute("src", url_base + "assets/img/icons/ico-top-cards.png");
	iconImg[1].setAttribute("src", url_base + "img/icons/ico-top-rojo-01.png");
	iconImg[2].setAttribute("src", url_base + "assets/img/icons/ico-top-cards.png");
	iconImg[3].setAttribute("src", url_base + "img/icons/ico-top-rojo-01.png");

	var imgCard = document.getElementsByClassName("imgCard");

	imgCard[0].setAttribute("src",url_base + "campanas/renueva-tu-auto-bmw/img/remueve-tu-auto-caluga-tc.jpg");
	imgCard[1].setAttribute("src",url_base + "campanas/descuento-starbucks/img/descuento-starbucks-caluga-tc.jpg");
	imgCard[2].setAttribute("src",url_base + "campanas/cuotizacion/img/cuotizacion-caluga-tc.jpg");
	imgCard[3].setAttribute("src",url_base + "campanas/cineplanet/img/cineplanet-caluga-tc.jpg");


	var sliderContainer = document.getElementById("sliderContainer");

	var data = tarjetas_content.filter( item => {return item.featured == true});

	data.forEach(function(i){
		let lboxi = document.createElement("div");
		lboxi.setAttribute("class","lboxi");

		let row = document.createElement("div");
		row.setAttribute("class", "row");

		let col12 = document.createElement("div");
		col12.setAttribute("class", "col-md-12");

		let h2 =  document.createElement("h2");
		h2.innerHTML = i.name;

		col12.appendChild(h2);
		row.appendChild(col12);

		let row2 = document.createElement("div");
		row2.setAttribute("class","row");

		let principal = document.createElement("div");
		principal.setAttribute("class","col-md-3 text-center cont-space-img");

		let imagen = document.createElement("img");
		imagen.setAttribute("src", url_base + i.imagen);
		imagen.setAttribute("class","img-responsive");

		let contenedorRenta = document.createElement("div");
		contenedorRenta.setAttribute("class","renta-minima");

		let h4 =  document.createElement("h5");
		let span = document.createElement("span");
		span.setAttribute("class","enta-valor");
		span.innerHTML = "Renta minima: $" + i.min_renta; + ".-";

		h4.appendChild(span);
		contenedorRenta.appendChild(h4);
		principal.appendChild(imagen);
		principal.appendChild(contenedorRenta);

		let principal2 =  document.createElement("div");
		principal2.setAttribute("class","col-md-6 text-left");

		let ul =  document.createElement("ul");

		// for
		i.ver_mas.forEach(function(o){
			let li = document.createElement("li");
			li.innerHTML = o.item;
			ul.appendChild(li);
		})
		
		// end for

		principal2.appendChild(ul);

		let principal3 = document.createElement("div");
		principal3.setAttribute("class","col-md-3 text-center");

		let h5 = document.createElement("h5");
		h5.innerText = "Costos de Mantención";
		let p = document.createElement("p");
		p.innerHTML = i.mantencion;

		let a = document.createElement("a");
		a.setAttribute("class","btn btn-default red solicitar");
		// funcion momentanea para el sitio cl viejo
		/*switch(i.idForm){ //Respaldo form antiguo
			case '11':
				a.setAttribute('href','/planes/plan-worldmember-limited.asp');
				break;
			case '9':
				a.setAttribute('href','/tarjetas/personas/formulario_tc/formulario.asp?tar=9');
				break;
			case '1':
				a.setAttribute('href','/tarjetas/personas/formulario_tc/formulario.asp?tar=1');
				break;
			case '3':
				a.setAttribute('href','/tarjetas/personas/formulario_tc/formulario.asp?tar=3');
				break;
			case '10':
				a.setAttribute('href','/tarjetas/personas/formulario_tc/formulario.asp?tar=10');
				break;
		}*/
		switch(i.idForm){
			case "11":
				a.setAttribute("href","http://www.santander.cl/planes/plan-worldmember-limited.asp");
				break;
			case "9":
				a.setAttribute("href","http://www.santander.cl/tarjetas/personas/formulario_tc/formulario.asp?tar=9");
				break;
			case "1":
				a.setAttribute("href","http://www.santander.cl/tarjetas/personas/formulario_tc/formulario.asp?tar=1");
				break;
			case "3":
				a.setAttribute("href","http://www.santander.cl/tarjetas/personas/formulario_tc/formulario.asp?tar=3");
				break;
			case "10":
				a.setAttribute("href","http://www.santander.cl/tarjetas/personas/formulario_tc/formulario.asp?tar=10");
				break;
		}
		//a.setAttribute('href','/tarjetas/tarjetas/formulario_tc/formulario.asp?tar='+data[0].idForm);
		a.innerText = "Solicitar";

		let a2 = document.createElement("a");
		a2.setAttribute("class","vermas see-more animated auto-cent");
		//a2.setAttribute('href', url_base + 'nuestras-tarjetas/' + i.link);
		a2.setAttribute("href", i.linkCL);
		a2.innerText = "Ver más";

		principal3.appendChild(h5);
		principal3.appendChild(p);
		principal3.appendChild(a);
		principal3.appendChild(a2);

		row2.appendChild(principal);
		row2.appendChild(principal2);
		row2.appendChild(principal3);

		lboxi.appendChild(row);
		lboxi.appendChild(row2);
		sliderContainer.appendChild(lboxi);
	});


	$(".space-card").slick({
	    autoplay:true,
	    autoplaySpeed:5000,
	    speed:600,
	    slidesToShow:1,
	    slidesToScroll:1,
	    pauseOnHover:false,
	    dots:true,
	    pauseOnDotsHover:true,
	    cssEase: "linear",
	   // fade:true,
	    draggable:true,
	  });


}
// funcion que cambia el valor del slider
function changeInput(){
	
	let range = document.getElementById("inputRange");
	let val = document.getElementById("rangeText");
	if(range.value == 1){
		let value = {text:"$400.000 a $699.999",val:1}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 2){
		let value = {text:"$700.000 a $999.999",val:2}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 3){
		let value = {text:"$1.000.000 a $1.299.999",val:3}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 4){
		let value = {text:"$1.300.000 a $1.599.999",val:4}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 5){
		let value = {text:"$1.600.000 a $1.899.999",val:5}
		val.innerText = value.text;

	}
	else if(range.value == 6){
		let value = {text:"$1.900.000 a $2.199.999",val:6}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 7){
		let value = {text:"$2.200.000 a $2.499.999",val:7}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 8){
		let value = {text:"$2.500.000 a $2.799.999",val:8}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 9){
		let value = {text:"$2.800.000 a $3.099.999",val:9}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 10){
		let value = {text:"$3.100.000 a $3.399.999",val:10}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 11){
		let value = {text:"$3.400.000 a $3.699.999",val:11}
		val.innerText = value.text;
		id = value.val;
	}
	else if(range.value == 12){
		let value = {text:"$3.700.000 a $3.999.999",val:12}
		val.innerText = value.text;
		id = value.val;
	}	
	else if(range.value == 13){
		let value = {text:"$4.000.000 a $4.300.000",val:13}
		val.innerText = value.text;
		id = value.val;
	}
}

// function redirect to forms
function redirect(){
	window.location = "/tarjetas/tarjetas-santander/encuentra-tu-tarjeta/index.asp?id="+id;
}

var search = document.getElementById("search");

search.addEventListener("click", redirect, false);

// function set Title
function setTitle(name){
    var data = tituloBanco.filter(item => { return item.name === name})
    var nuestrasTarjetas = $("title");
    nuestrasTarjetas.text(data[0].value);
};