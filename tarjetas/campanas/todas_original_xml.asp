<!--#include virtual="/tarjetas/tarjetas/inc_files/header.asp"-->
<div class="breadcrumbs white-bg">
	<div class="container">
		<p><a href="<%=url_relativa %>" class="" title="Enlace a Tarjetas">Tarjetas</a><i>/</i><a class="current">Promociones</a></p>
	</div>
</div>
<main class="container page-promos">
<div class="tit-filtro">
<% filtro = Request("filtro")
			 %>
				<h2>Conoce nuestras promociones</h2>
                	<div class="select">
                    	<form action="" method="get">
                            <select id="cmbTarjetas" name="filtro" onchange="submit()">
                                <option value="" <% if filtro = "" then %> selected="selected"<% end if %>>Ver todas las promociones</option>
                                <option value="superpuntos" <% if filtro = "superpuntos" then %> selected="selected"<% end if %>>SUPERPUNTOS</option>
                                <option value="latampass" <% if filtro = "latampass" then %> selected="selected"<% end if %>>LATAM Pass</option>
                                <option value="clublectores" <% if filtro = "clublectores" then %> selected="selected"<% end if %>>Club de lectores</option>
                                <option value="otros" <% if filtro = "otros" then %> selected="selected"<% end if %>>Otros</option>
                            </select>
                       </form>
                    </div>
			</div>
			
<%=xsltransform(dir_relativo & "inc_files\xml\campanas.xml", dir_relativo & "inc_files\xsl\campanas_calugas.xsl", "", "", "tipo", "all", filtro )  %>	

</main>
<!-- #include virtual="/tarjetas/tarjetas/inc_files/footer.asp" -->
