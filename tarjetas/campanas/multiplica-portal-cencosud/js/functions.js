$(document).ready(function(){
	$('.botonModal').click(function() {
        $(this).next().removeClass('bgModalHide');
        $(this).next().next().removeClass('modalSimple');
        $('body').css('overflow','hidden');
    });
    $('.cerrarModal').click(function() {
        $(".bgModal").addClass('bgModalHide');
        $(".contenedorModal").addClass('modalSimple');
        $('body').css('overflow','scroll');
    });
    $('.contenedorModal').click(function() {
        $(".bgModal").addClass('bgModalHide');
        $(".contenedorModal").addClass('modalSimple');
        $('body').css('overflow','scroll');
    });
    $('.contenedorModalSimple').click(function(event){
        event.stopPropagation();
    });
	
	function acordeon(){
		$('.block .contenedorAcordeon').hide();
		$('.accordion h4').on('click',function(){
			var is_active = false;
			if($(this).find("span.icon-flecha-abajo").hasClass('flechaActiva')){
				is_active = true;
			}
			$('.accordion h4 span.icon-flecha-abajo').removeClass("flechaActiva");
			if(!is_active){
				$(this).find("span.icon-flecha-abajo").addClass("flechaActiva");
			}
			if($(this).next().find('.contenedorAcordeon').is(':visible')){
				$(this).next().find('.contenedorAcordeon').slideUp();
			}
			if($(this).next().find('.contenedorAcordeon').is(':hidden')){
				$('.accordion h4').next().find('.contenedorAcordeon').slideUp();
				$(this).next().find('.contenedorAcordeon').slideDown();
			}
		});
	}
	
    acordeon();
});