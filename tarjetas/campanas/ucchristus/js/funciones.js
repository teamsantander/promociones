// JavaScript Document

function inicia_tab(){
    var first_tab = $("ul.tabs_buttons li:first-child > a");
    first_tab.addClass("active")
    var primer_destino = first_tab.data("target");
        $(primer_destino).attr("style", "display:block");
    $("ul.tabs_buttons li > a").click(function(){
        $("ul.tabs_buttons li > a").removeClass("active")
        $(".contenido-tab").removeAttr("style")
        var destino = $(this).data("target");
        $(destino).attr("style", "display:block");
        $(this).addClass("active");
    })
}
function acordeon(){
    $(".block .contenedorAcordeon").hide();
    $(".accordion h4").on("click",function(){
        var is_active = false;
        if($(this).find("span.icon-flecha-abajo").hasClass("flechaActiva")){
            is_active = true;
        }
        $(".accordion h4 span.icon-flecha-abajo").removeClass("flechaActiva");
        if(!is_active){
            $(this).find("span.icon-flecha-abajo").addClass("flechaActiva");
        }
        if($(this).next().find(".contenedorAcordeon").is(':visible')){
            $(this).next().find(".contenedorAcordeon").slideUp();
        }
        if($(this).next().find(".contenedorAcordeon").is(':hidden')){
            $(".accordion h4").next().find(".contenedorAcordeon").slideUp();
            $(this).next().find(".contenedorAcordeon").slideDown();
        }
    });
}

function responsiveTab(){ 
        $(".tabs_buttons").each(function(){
                var acordeoHtml = "<div class='accordion accTab'>";
                var titulos = $(this).find("li").find("a");
                titulos.each(function(){
                    var target = $(this).data("target");
                    var titulo = $(this).text();
                    acordeoHtml += "<article>";
                    acordeoHtml += "<h4>"+titulo+"<span class='icon-flecha-abajo flecha'></span></h4>";
                    acordeoHtml += "<div class='block'><div class='contenedorAcordeon' style='display: none;'>" + $(target).html() + "</div></div>";
                    acordeoHtml += "</article>";
                });
                acordeoHtml += "<article>";
                $(this).parent().html($(this).parent().html() + acordeoHtml);
                //acordeon();
        });
        if(window.innerWidth < 650){
            $(".tabs_buttons").each(function(){
                $(this).hide();
            });
            $(".tabs_content").each(function(){
                $(this).hide();
            });
            $(".accTab").each(function(){
                $(this).show();
            });
        }else{
            $(".tabs_buttons").each(function(){
                $(this).show();
            });
            $(".tabs_content").each(function(){
                $(this).show();
            });
            $('.accTab').each(function(){
                $(this).hide();
            });
        }
        inicia_tab();
    }
    window.onresize = function(event) {
        if(window.innerWidth < 650){
            $('.tabs_buttons').each(function(){
                $(this).hide();
            });
            $('.tabs_content').each(function(){
                $(this).hide();
            });
            $('.accTab').each(function(){
                $(this).show();
            });
        }else{
            $('.tabs_buttons').each(function(){
                $(this).show();
            });
            $('.tabs_content').each(function(){
                $(this).show();
            });
            $('.accTab').each(function(){
                $(this).hide();
            });
        }
    }
$( document ).ready(function() {
    responsiveTab();
	acordeon();
});