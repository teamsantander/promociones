<%
	Set ini 	 = objXMLDoc.documentElement
	Set NodeList = objXMLDoc.getElementsByTagName("campana/" & campana) 'Ej: campana = v_revuelo_nacional.

	For each nodeCampana In NodeList
		Set rutacss = nodeCampana.getElementsByTagName("mirutacss")(0)
		Set rutaxml = nodeCampana.getElementsByTagName("mirutaxml")(0)
		'Set nombre 	= nodeCampana.getElementsByTagName("nombre")(0)
	Next


	If rutaxml.text = "" Then
		Response.Write("Falta XML contenido")
	Else
		Set objXMLDoc 		= Server.CreateObject("Microsoft.XMLDOM")
			objXMLDoc.async = False
			objXMLDoc.load(Server.MapPath("\") & "\tarjetas\personas\campanas\" & rutaxml.text)

		If objXMLDoc.parseError.errorcode <> 0 Then
			Response.Write(objXMLDoc.parseError.errorcode)
		End If

		Set ini 	 = objXMLDoc.documentElement
		Set NodeList = objXMLDoc.getElementsByTagName("campana/" & campana)

		For Each nodeCampana In NodeList
			Set nombre 				= nodeCampana.getElementsByTagName("nombre")(0)
			Set descripcion 		= nodeCampana.getElementsByTagName("descripcion")(0)
			Set banner_campana 		= nodeCampana.getElementsByTagName("banner")(0)
			Set login_campana 		= nodeCampana.getElementsByTagName("formulario")(0)
			Set avisolegal_campana	= nodeCampana.getElementsByTagName("avisolegal")(0)
			Set url 				= nodeCampana.getElementsByTagName("url")(0)
			Set bajada 				= nodeCampana.getElementsByTagName("bajada")(0)
			Set solicitar 			= nodeCampana.getElementsByTagName("solicitar")(0)
			Set bases 				= nodeCampana.getElementsByTagName("bases")(0)
			Set face 				= nodeCampana.getElementsByTagName("face")(0)
		Next
	End If


	If (nombre.text = "Navidad LATAM Pass") Then
		keys_campana = "navidad, kilómetros, compras, vuela, créditos, banco, chile, LATAM Pass, Santander, volar, comprar, viajar, conocer, vacaciones, crédito"
		key_meta_desc = "Adelanta tus compras en esta Navidad,  y vuela antes acumulando el doble de Kilómetros LATAM Pass, con tus tarjetas de crédito de Banco Santander"
	End If


	servidor = "http://"&Request.ServerVariables("SERVER_NAME")
	carga = carga & "<meta property='og:title' content='" & nombre.text & "'/>" & vbCrLf
	carga = carga & "<meta property='og:type' content='website' />" & vbCrLf
	carga = carga & "<meta property='og:url' content='" & servidor & url.text & "?o=fb'>" & vbCrLf
	carga = carga & "<meta property='og:image' content='/css/bitmaps/santander_facebook.jpg'>" & vbCrLf
	carga = carga & "<meta property='og:site_name' content='Santander'/>" & vbCrLf
	carga = carga & "<meta property='og:description' content='" & bajada.text & "'/>" & vbCrLf
	carga = carga & "<meta property='fb:admins' content='" & face.text &"'/>" & vbCrlf

	'url_short = bitlyThis(servidor & url.text)
%>
<script type="text/javascript" src="/includes/js/facebook.js"></script>
<script type="text/javascript" src="/tarjetas/tarjetas/include/js/funciones_campana.js"></script>

<div id="fb-root"></div>



<% For each nodeCampana in NodeList %>
<div class="breadcrumbs white-bg">
			<div class="container">
				<p><span>Tarjetas</span><i>/</i><span>Campañas</span><i>/</i><a class="current" href="#" title="Enlace a..."><%=nombre.text%></a></p>
				<ul class="grey-share-icons">
					<li><a class="twitter" href="#" title="Enlace a Twitter oficial de Santander"></a></li>
					<li><a class="facebook" href="#" title="Enlace a Facebook oficial de Santander"></a></li>
					<li><a class="youtube" href="#" title="Enlace a Youtube oficial de Santander"></a></li>
				</ul>
			</div>
		</div>
<main class="container">
	<div class="tit-filtro">
				<h2><%=nombre.text%></h2>
			</div>
<section class="inner-container">
				
	<div class="row">
			

            <%=descripcion.text%>
       
		
		

        <div class="aside_rutclave" autocomplete="off" <% If banner_campana.text <> empty Then %>style="background-image: url(<%=banner_campana.text%>)" <% End If %>>
        	<%

				If login_campana.text <> "si" Then
					login = Replace(login_campana.text, "{servidor}", SecWebname)
        			Response.Write(login)
				End If
			%>
        </div>





        <% If bases.text <> empty Then %>
    	<div id="bases">
    		<%=bases.text%>
    	</div>
        <% End If %>


    </div>

    <a href="#" class="link red auto-cent" title="Ver más" data-show-target="legal"><span>Condiciones y restricciones</span> <i class="icon-element small-down-arrow-icon"></i></a>
					
	<div class="legal hidden-target mar-top-40 ohidden" data-show="legal"><%=avisolegal_campana.text%></div>
	<div class="oval-feed mar-bottom-40">
	<p>Comparte esta promoción</p>
	<ul>
		<li><a href="https://twitter.com/santanderchile" class="" title="Enlace a Twitter oficial de Santander"><i class="twitter"></i></a></li>
		<li><a href="https://www.facebook.com/santanderchile" class="" title="Enlace a Facebook oficial de Santander"><i class="facebook"></i></a></li>
		<li><a href="https://www.youtube.com/user/SantanderChile" class="" title="Enlace a Youtube oficial de Santander"><i class="youtube"></i></a></li>
	</ul>
</div>



</div>
</section>

</main>
<%
	Next

%>
