<!--#include virtual="/tarjetas/inc_files/header.asp"-->
<script type="text/javascript" src="/data/collections_tarjetas.js"></script>
<script type="text/javascript" src="/data/promociones.js"></script>
<div class="breadcrumbs white-bg">
	<div class="container">
		<p><a href="<%=url_relativa %>" class="" title="Enlace a Tarjetas">Tarjetas</a><i>/</i><a class="current">Promociones</a></p>
	</div>
</div>
<main class="container page-promos">
    <div class="tit-filtro">
     <h2>Conoce nuestras promociones</h2>
        <div class="select">
                <select id="cmbTarjetas" name="filtro">
                       <option value="todos" selected="selected">Ver todas las promociones</option>
                       <option value="superpuntos" >SUPERPUNTOS</option>
                       <option value="latampass" >LATAM Pass</option>
                       <option value="clublectores">Club de lectores</option>
                       <option value="otros">Otros</option>
                </select>
        </div>
	</div>

   <section class="inner-container">
        <h2 class="subtitle home-subtitle"></h2>
        <div class="cont_calugas 2" data-current="14-08-2017">
            <div class="row clearfix" id="calugas">

            </div>
        </div>
    </section> 
</main>
<!-- #include virtual="/tarjetas/inc_files/footer.asp" -->
