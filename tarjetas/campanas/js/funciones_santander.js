var url_dim = localStorage.getItem("url_dinamica");
$(document).ready(function(){
	setTitle("promo");
	//Cargamos las promociones
	$.each(tarjetas, function(index, item) {
	 if(item.url_out === "si"){
	 	var url_out = item.a_href;
	 } else {
	 	var url_out = url_dim + item.a_href;
	 }
	 if(item.activado === 0){
	 	//No tiene nada
	 }else{     	
	       $("#calugas").append([

	       	   "<div class='contenedor-caluga col-sm-3' data-inicio='"+item.Fecha_inicio+"' data-fin='"+item.Fecha_fin+"'>",
					"<div class='caluga'>",
						"<div class='thumbs'>",
							"<a href='"+url_out+"' title='ir a ' onclick='"+item.Onclick+"' style='overflow:hidden'>",
							    "<img src='"+url_dim+item.img_icon+"' alt='"+item.Nombre_tarjeta+"' class='icon-top'>",
								"<span class='text-icon "+url_dim+item.icon_tarjeta+" '>"+item.text_icon+"</span>",
								"<img src='"+url_dim+item.img_caluga+"' alt='' style='height:132px; width:auto;'>",
							"</a>",
						"</div>",
						"<div class='info eq-height' style='height: 113px;'>",
							"<h2 class='h3'>",
								"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
									''+item.Nombre_tarjeta+'', 
								"</a>",
							"</h2>",
							"<p style='vertical-align:none'>",
								"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
								 ''+item.Bajada+'',  
								"</a>",
							"</p>",
							"<div class='text-right'>",
								"<div class='text-right'>",
								 "<a href='"+url_out+"' class='to-the-right see-more' onclick='"+item.Onclick+"'>",
								 "Ver Más",
								 "</a>",
								"</div>",
							"</div>",
						"</div>",
					"</div>",
				  "</div>"

	       	 ].join(""));
   		}
    });

	//Filtramos
	$("#cmbTarjetas").change(function(){
		var valor = $(this).val();
	    console.log(valor);
		if(valor === "todos"){
			mostrar_todos();
		}else{
			filtro_activado(valor);
		}
	});

});

// function para el filtro
function filtro_activado(valor){
	$("#calugas").empty(); //Borramos lo que esta en el html
	$.each(tarjetas, function(index, item) { //Recorremos todo las tarjetas
		if(item.url_out === "si"){
		 	var url_out = item.a_href;
		 } else {
		 	var url_out = url_dim + item.a_href;
		 }
	
		 if(valor == item.filtro){ //Preguntamos si valor (lo seleccionado por el select) es igual al item.filtro 
		 	  if(item.activado === 0){

		 	  }else{
				 $("#calugas").append([ //Mostramos todo los elementos

		       	   "<div class='contenedor-caluga col-sm-3' data-inicio='"+item.Fecha_inicio+"' data-fin='"+item.Fecha_fin+"'>",
						"<div class='caluga'>",
							"<div class='thumbs'>",
								"<a href='"+url_out+"' title='ir a ' onclick='"+item.Onclick+"' style='overflow:hidden'>",
								    "<img src='"+url_dim+item.img_icon+"' alt='"+item.Nombre_tarjeta+"' class='icon-top'>",
									"<span class='text-icon "+url_dim+item.icon_tarjeta+" '>"+item.text_icon+"</span>",
									"<img src='"+url_dim+item.img_caluga+"' alt='' style='height:132px; width:auto;'>",
								"</a>",
							"</div>",
							"<div class='info eq-height' style='height: 113px;'>",
								"<h2 class='h3'>",
									"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
										''+item.Nombre_tarjeta+'', 
									"</a>",
								"</h2>",
								"<p style='vertical-align:none'>",
									"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
									 ''+item.Bajada+'',  
									"</a>",
								"</p>",
								"<div class='text-right'>",
									"<div class='text-right'>",
									 "<a href='"+url_out+"' class='to-the-right see-more' onclick='"+item.Onclick+"'>",
									 "Ver Más",
									 "</a>",
									"</div>",
								"</div>",
							"</div>",
						"</div>",
					  "</div>"
				].join(""));
			 }
		  }
	   
	});

}

function mostrar_todos(){
	$("#calugas").empty(); //Borramos lo que esta para mostrar todos!
	$.each(tarjetas, function(index, item) { //Recorremos todo los elementos
		if(item.url_out === "si"){
	 		var url_out = item.a_href;
		} else {
		 	var url_out = url_dim + item.a_href;
		}
		if(item.activado === 0){
	 	//No tiene nada
	 	}else{  
			 $("#calugas").append([ //Mostramos todos

	       	   "<div class='contenedor-caluga col-sm-3' data-inicio='"+item.Fecha_inicio+"' data-fin='"+item.Fecha_fin+"'>",
					"<div class='caluga'>",
						"<div class='thumbs'>",
							"<a href='"+url_out+"' title='ir a ' onclick='"+item.Onclick+"' style='overflow:hidden'>",
							    "<img src='"+url_dim+item.img_icon+"' alt='"+item.Nombre_tarjeta+"' class='icon-top'>",
								"<span class='text-icon "+item.icon_tarjeta+" '>"+item.text_icon+"</span>",
								"<img src='"+url_dim+item.img_caluga+"' alt='' style='height:132px; width:auto;'>",
							"</a>",
						"</div>",
						"<div class='info eq-height' style='height: 113px;'>",
							"<h2 class='h3'>",
								"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
									''+item.Nombre_tarjeta+'', 
								"</a>",
							"</h2>",
							"<p style='vertical-align:none'>",
								"<a href='"+url_dim+item.a_href+"' title='ir a "+item.Nombre_tarjeta+"' onclick='"+item.Onclick+"'>", 
								 ''+item.Bajada+'',  
								"</a>",
							"</p>",
							"<div class='text-right'>",
								"<div class='text-right'>",
								 "<a href='"+url_out+"' class='to-the-right see-more' onclick='"+item.Onclick+"'>",
								 "Ver Más",
								 "</a>",
								"</div>",
							"</div>",
						"</div>",
					"</div>",
				  "</div>"
			].join(""));
		}
	 });
}

function setTitle(name){

	var data = tituloBanco.filter(item => { return item.name === name})
	var nuestrasTarjetas = $("title");
	nuestrasTarjetas.text(data[0].value);	

};