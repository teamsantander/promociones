<!--#include virtual="/include/inc/sitioseg.inc"-->
<%
' ** Descripción    : Nuevo Header   		  **
' ** Disenador      : Continuidad Santander   **
' ** Programador    : Continuidad Santander   **
' ** Fecha          : 05-04-2017              **
' ** Versión        : 1.0                     **
%>
	<!--[if lte IE 7]>
	<div class="menuIE7">
		<div class="principalIE7">
			<a class="logoLink" href=""><img src="" alt="BANCO SANTANDER"></a>
			<div class="contenedorBotones">
				<a class="botonMenuIE7" href="/index.asp">Personas</a>
				<a class="botonMenuIE7" href="/select/index.asp">Select</a>
				<a class="botonMenuIE7" href="/advance/index.asp">Pymes</a>
				<a class="botonMenuIE7" href="/empresas/index.asp">Empresas</a>
				<a class="botonMenuIE7" href="http://www.santanderpb.cl">Private Banking</a>
				<a class="botonMenuIE7" href="/GCB/index.asp" target="_blank">GCB</a>
				<a class="botonMenuIE7" href="/universidades/index.asp">Universidades</a>
				<span class="botonMenuIE7 ayuda" >AA
					<ul class="submenuAyuda displayNone"><div class="flechaAyuda"></div><li><a class="preguntasFrecuentes preguntasFrecuentesIE7" href="">Preguntas frecuentes</a></li><li><a class="chatOnline chatOnlineIE7" href="">Chat online</a></li><li><a class="vox voxIE7" href="">Ll&aacute;manos al 600 320 3000</a></li><li><a class="servicioCliente servicioClienteIE7" href="/servicio_al_cliente/index.asp">Servicio al cliente</a></li><li><a class="salaPrensa salaPrensaIE7" href="https://saladecomunicacion.santander.cl/" target="_blank">Sala de prensa</a></li></ul>
				</span>
				<a class="botonMenuIE7 beneficiosIE7" href="https://mov.santander.cl/beneficios?segmento=s-personas"><img src="/hyf_html/css/bitmaps/boton-beneficios.png" alt=""></a>
			</div>
		</div>
		<div class="secundarioIE7">
			<div class="cajaCentrar">
				<div class="contenedorBotones">
					<ul id="submenuPersonas" class="displayNone">
						<li><a href="/nuestro_banco/index.asp">Nuestro Banco</a></li>
						<li><a href="/nuestros_productos/index.asp">Nuestros Productos</a></li>
						<li><a href="/simuladores/credito_consumo/simulador.asp">Cr&eacute;dito Personal</a></li>
						<li><a href="/tarjetas/index.asp">Tarjetas</a></li>
						<li><a href="/seguros/index.asp">Seguros</a></li>
						<li><a href="/inversiones/index.asp">Inversiones</a></li>
						<li><a href="/hipotecario/index.asp">Mundo Hipotecario</a></li>
						<li><a class="" href="/cuenta-corriente.asp">Hazte Cliente</a></li>
					<div class="clearIE7"></div>
					</ul>
					<ul id="submenuSelect" class="displayNone">
						<li><a href="/select/select.asp">Santander Select</a></li>
						<li><a href="/select/atencion.asp">Atenci&oacute;n</a></li>
						<li><a href="/select/productos.asp">Productos</a></li>
						<li><a href="/select/inversion.asp">Inversiones</a></li>
						<li><a href="/select/financiamiento.asp">Financiamiento</a></li>
						<li><a href="/select/proteccion.asp">Seguros</a></li>
						<li><a href="/select/santander_experience.asp">Select Experience</a></li>
						<li><a href="/select/global-value/index.asp">Global Value</a></li>
						<li><a class="" href="/select/cliente_select.asp">Hazte Cliente</a></li>
					<div class="clearIE7"></div>
					</ul>
					<ul id="submenuAdvance" class="displayNone">
						<li><a href="/advance/financiamiento.asp">Financiamiento <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="/advance/financiamiento/comprar-inventario.asp">Comprar inventario</a>
								<a href="/advance/financiamiento/comprar-maquina-o-vehiculo.asp">Comprar m&aacute;quina o vehículo</a>
								<a href="/advance/financiamiento/comprar-propiedad-o-terreno.asp">Comprar propiedad o terreno</a>
								<a href="/advance/financiamiento/financiar-comercio-exterior.asp">Financiar comercio exterior</a>
								<a href="/advance/financiamiento/pagar-mano-de-obra.asp">Pagar mano de obra</a>
								<a href="/advance/financiamiento/respaldar-licitaciones.asp">Respaldar licitaciones</a>
							</div>
						</li>
						<li><a href="/advance/tarjetas.asp">Tarjetas <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="tarjetas/worldmember-business-latam-pass.asp">Worldmember Business LATAM Pass</a>
								<a href="tarjetas/santander-advance-lanpass.asp">Santander Empresas LATAM Pass</a>
								<a href="tarjetas/santander-advance-empresarial.asp">Santander Empresas</a>
							</div>
						</li>
						<li><a href="/advance/seguros.asp">Seguros <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="/advance/seguros-pyme/seguro-auto-flota-advance.asp">Seguro auto flota</a>
								<a href="/advance/seguros-pyme/colectivo-de-accidentes-personales.asp">Seguro colectivo de accidentes personales</a>
								<a href="/advance/seguros-pyme/seguro-fraude-plus.asp">Seguro fraude plus</a>
								<a href="/advance/seguros-pyme/seguro-multirriesgo-advance.asp">Seguro multirriesgo</a>
								<a href="/advance/seguros-pyme/vehiculos-pesados.asp">Seguro veh&iacute;culos pesados</a>
							</div>
						</li>
						<li><a href="/advance/inversiones-y-derivados.asp">Inversiones <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="/advance/inversiones-y-derivados/acciones.asp">Acciones</a>
								<a href="/advance/inversiones-y-derivados/depositos.asp">Dep&oacute;sitos</a>
								<a href="/advance/inversiones-y-derivados/productos-derivados.asp">Derivados</a>
								<a href="/advance/inversiones-y-derivados/fondos-mutuos.asp">Fondos mutuos</a>
							</div>
						</li>
						<li><a href="/advance/productos-comex.asp">Comercio Exterior <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="/advance/productos-comex/garantias-moneda-extranjera.asp">Boleta de garant&iacute;a</a>
								<a href="/advance/productos-comex/cuenta-corriente-moneda-extranjera.asp">Cuenta corriente</a>
								<a href="/advance/productos-comex/carta-credito-importacion.asp">Carta de cr&eacute;dito de importaci&oacute;n</a>
								<a href="/advance/productos-comex/cobranzas-de-exportacion.asp">Cobranzas de exportaci&oacute;n</a>
								<a href="/advance/productos-comex/cobranzas-de-importacion.asp">Cobranzas de importaci&oacute;n</a>
								<a href="/advance/productos-comex/ordenes-pago-enviada.asp">Orden de pago enviada</a>
								<a href="/advance/productos-comex/ordenes-pago-recibida.asp">Orden de pago recibida</a>
							</div>
						</li>
						<li><a href="/advance/hazte-cliente.asp">Hazte Cliente</a></li>
					</ul>
					<ul id="submenuEmpresas" class="displayNone">
						<li><a href="/empresas/nuestros_productos/index.asp">Nuestros Productos</a></li>
						<li><a href="/empresas/nuestros_productos/financiamiento.asp">Solicita tu Cr&eacute;dito</a></li>
						<li><a href="/tarjetas/empresas/nuestras-tarjetas/index.asp">Tarjetas</a></li>
						<li><a href="/empresas/garantias_estatales/index.asp">Garant&iacute;as Estatales</a></li>
						<li><a href="/empresas/pagos_en_linea/index.asp">Pagos por Internet</a></li>
						<li><a href="/empresas/clientes-internacionales/index.asp">Clientes Internacionales <span class="flechaAbajoIE">M</span></a>
							<div class="desplegableIE7">
								<a href="/empresas/clientes-internacionales/international-desk/index.asp">International Desk</a>
								<a href="/empresas/clientes-internacionales/santander-passport/index.asp">Santander passport</a>
								<a href="/empresas/clientes-internacionales/santander-trade/index.asp">Santander Trade</a>
								<a href="/empresas/clientes-internacionales/santander-trade-network/index.asp">Santander Trade Network</a>
							</div>
						</li>
						<li><a class="" href="/empresas/hazte_cliente/plan-empresas.asp">Hazte Cliente</a></li>
					</ul>
					<ul id="submenuPrivate" class="displayNone">
						<li class="menuVacio"></li>
					</ul>
					<ul id="submenuUniversidades" class="displayNone">
						<li class="menuVacio"></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<![endif]-->
<div class="menuSantander">
	<div class="menuSantanderEscritorio">
		<div id="take_class" class="principal">
			<ul>
				<a class="logoLink" href=""><img src="" alt="BANCO SANTANDER"></a>
				<li><a id="" href="/index.asp">Personas</a></li><li><a id="" href="/select/index.asp">Select</a></li><li><a id="" href="/advance/index.asp">Pymes</a></li><li><a id="" href="/empresas/index.asp">Empresas</a></li><li><a id="" href="http://www.santanderpb.cl">Private Banking</a></li><li><a href="/GCB/index.asp" target="_self">GCB</a></li><li><a id="" href="/universidades/index.asp">Universidades</a></li><li><a class="ayuda">i<span class="icon-ayuda"></span></a>
				<ul id="submenuAyudaPersonas" class="submenuAyuda displayNone">
					<li>
						<a class="preguntasFrecuentes" href=""><span class="icon-pregunta icono"></span>Preguntas frecuentes<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="chatOnline" href=""><span class="icon-chat-online icono"></span>Chat online<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="vox" href=""><span class="icon-superlinea icono"></span>Ll&aacute;manos al 600 320 3000<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="servicioCliente" href="/servicio_al_cliente/index.asp"><span class="icon-servicio-cliente icono"></span>Servicio al cliente<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="salaPrensa" href="https://saladecomunicacion.santander.cl/" target="_blank"><span class="icon-sala-prensa icono"></span>Sala de prensa<span class="icon-flecha-derecha"></span></a>
					</li>
				</ul>
				<!--
				<ul id="submenuAyudaAdvance" class="submenuAyuda displayNone">
					<li>
						<a class="vox" href=""><span class="icon-superlinea icono"></span>Ll&aacute;manos al 600 320 3000<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="sucursales" href=""><span class="icon-sucursales icono"></span>Sucursales<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="servicioCliente" href="/servicio_al_cliente/index.asp"><span class="icon-servicio-cliente icono"></span>Servicio al cliente<span class="icon-flecha-derecha"></span></a>
					</li>
					<li>
						<a class="preguntasFrecuentes" href=""><span class="icon-pregunta icono"></span>Preguntas frecuentes<span class="icon-flecha-derecha"></span></a>
					</li> 
					<li>
						<a class="salaPrensa" href="https://saladecomunicacion.santander.cl/" target="_blank"><span class="icon-sala-prensa icono"></span>Sala de prensa<span class="icon-flecha-derecha"></span></a>
					</li>
				</ul>
				--> 
				</li><li><a href="https://mov.santander.cl/beneficios?segmento=s-personas" class="btnBeneficios"><span class="icon-cupones iconoBeneficios"></span>Beneficios</a></li>
			</ul>
		</div>
		<div class="submenuSantander">
			<ul id="submenuPersonas" class="displayNone">
				<li><a class="" href="/cuenta-corriente.asp"><span class="icon-servicio-cliente"></span>Hazte Cliente</a></li><li><a href="/nuestro_banco/index.asp">Nuestro Banco</a></li><li><a href="/nuestros_productos/index.asp">Nuestros Productos</a></li><li><a href="/simuladores/credito_consumo/simulador.asp">Cr&eacute;dito Personal</a></li>

				<li><a href="/tarjetas/index.asp">Tarjetas <span class="icon-flecha-abajo"></span></a> 
					<ul class="submenuDesplegable">
						<li><a href="http://www.santander.cl/tarjetas/personas/nuestras-tarjetas/index.asp">Nuestras tarjetas</a></li>
						<!--li><a href="/tarjetas/encuentra-tu-tarjeta/">Encuentra tu tarjeta</a></li-->
						<li><a href="/tarjetas/preguntas-frecuentes/index.asp">Preguntas frecuentes</a></li>
						<li><a href="/tarjetas/campanas/index.asp">Promociones</a></li>
						<li><a href="https://mov.santander.cl/beneficios?segmento=s-personas">Beneficios</a></li>
						<li><a href="https://superpuntos.santander.cl/ilionX45/Custom/SAN.CHL/Customers/StoreIndex.aspx" target="_blank">Canjear SUPERPUNTOS</a></li>
					</ul>
				</li>


				<li><a href="/seguros/index.asp">Seguros</a></li><li><a href="/inversiones/index.asp">Inversiones</a></li><li><a href="/hipotecario/index.asp">Mundo Hipotecario</a></li>
			</ul>
			<ul id="submenuSelect" class="displayNone">
				<li><a class="" href="/cuenta-corriente.asp"><span class="icon-servicio-cliente"></span>Hazte Cliente</a></li><li><a href="/select/select.asp">Santander Select</a></li><li><a href="/select/atencion.asp">Atenci&oacute;n</a></li><li><a href="/select/productos.asp">Productos</a></li><li><a href="/select/inversion.asp">Inversiones</a></li><li><a href="/select/financiamiento.asp">Financiamiento</a></li><li><a href="/select/proteccion.asp">Seguros</a></li><li><a href="/select/santander_experience.asp">Select Experience</a></li><li><a href="/select/global-value/index.asp">Global Value</a></li>
			</ul>
			<ul id="submenuAdvance" class="displayNone">
				<li><a href="/advance/hazte-cliente/index.asp"><span class="icon-servicio-cliente"></span>Hazte Cliente</a></li>
				<li><a href="/advance/financiamiento.asp">Financiamiento <span class="icon-flecha-abajo"></span></a>
				<ul class="submenuDesplegable">
					<li><a href="/advance/financiamiento/comprar-inventario.asp">Comprar inventario</a></li>
					<li><a href="/advance/financiamiento/comprar-maquina-o-vehiculo.asp">Comprar m&aacute;quina o vehículo</a></li>
					<li><a href="/advance/financiamiento/comprar-propiedad-o-terreno.asp">Comprar propiedad o terreno</a></li>
					<li><a href="/advance/financiamiento/financiar-comercio-exterior.asp">Financiar comercio exterior</a></li>
					<li><a href="/advance/financiamiento/pagar-mano-de-obra.asp">Pagar mano de obra</a></li>
					<li><a href="/advance/financiamiento/respaldar-licitaciones.asp">Respaldar licitaciones</a></li>
				</ul>
				</li>
				<li><a href="/advance/tarjetas.asp">Tarjetas <span class="icon-flecha-abajo"></span></a>
					<ul class="submenuDesplegable"> 																								
						<li><a href="tarjetas/worldmember-business-latam-pass.asp">Worldmember Business LATAM Pass</a></li> 
						<li><a href="tarjetas/santander-advance-lanpass.asp">Santander Empresas LATAM Pass</a></li> 
						<li><a href="tarjetas/santander-advance-empresarial.asp">Santander Empresas</a></li>  
					</ul>
				</li>
				<li><a href="/advance/seguros.asp">Seguros <span class="icon-flecha-abajo"></span></a>
				<ul class="submenuDesplegable">
					<li><a href="/advance/seguros-pyme/seguro-auto-flota-advance.asp">Seguro auto flota</a></li>
					<li><a href="/advance/seguros-pyme/colectivo-de-accidentes-personales.asp">Seguro colectivo de accidentes personales</a></li>
					<li><a href="/advance/seguros-pyme/seguro-fraude-plus.asp">Seguro fraude plus</a></li>
					<li><a href="/advance/seguros-pyme/seguro-multirriesgo-advance.asp">Seguro multirriesgo</a></li>
					<li><a href="/advance/seguros-pyme/vehiculos-pesados.asp">Seguro veh&iacute;culos pesados</a></li>
				</ul>
				</li>
				<li><a href="/advance/inversiones-y-derivados.asp">Inversiones <span class="icon-flecha-abajo"></span></a>
				<ul class="submenuDesplegable">
					<li><a href="/advance/inversiones-y-derivados/acciones.asp">Acciones</a></li>
					<li><a href="/advance/inversiones-y-derivados/depositos.asp">Dep&oacute;sitos</a></li>
					<li><a href="/advance/inversiones-y-derivados/productos-derivados.asp">Derivados</a></li>
					<li><a href="/advance/inversiones-y-derivados/fondos-mutuos.asp">Fondos mutuos</a></li>
				</ul>
				</li>
				<li><a href="/advance/productos-comex.asp">Comercio Exterior <span class="icon-flecha-abajo"></span></a>
				<ul class="submenuDesplegable">
					<li><a href="/advance/productos-comex/garantias-moneda-extranjera.asp">Boleta de garant&iacute;a</a></li>
					<li><a href="/advance/productos-comex/cuenta-corriente-moneda-extranjera.asp">Cuenta corriente</a></li>
					<li><a href="/advance/productos-comex/carta-credito-importacion.asp">Carta de cr&eacute;dito de importaci&oacute;n</a></li>
					<li><a href="/advance/productos-comex/cobranzas-de-exportacion.asp">Cobranzas de exportaci&oacute;n</a></li>
					<li><a href="/advance/productos-comex/cobranzas-de-importacion.asp">Cobranzas de importaci&oacute;n</a></li>
					<li><a href="/advance/productos-comex/ordenes-pago-enviada.asp">Orden de pago enviada</a></li>
					<li><a href="/advance/productos-comex/ordenes-pago-recibida.asp">Orden de pago recibida</a></li>
				</ul>
				</li>
				<li><a href="/advance/pagos-en-linea/index.asp">Servicios</a></li>
			</ul>
			<ul id="submenuEmpresas" class="displayNone">
				<li><a class="" href="/cuenta-corriente.asp"><span class="icon-servicio-cliente"></span>Hazte Cliente</a></li>
				<li><a href="/empresas/nuestros_productos/index.asp">Nuestros Productos</a></li><li><a href="/empresas/nuestros_productos/financiamiento.asp">Solicita tu Cr&eacute;dito</a></li><li><a href="/tarjetas/empresas/nuestras-tarjetas/index.asp">Tarjetas</a></li><li><a href="/empresas/garantias_estatales/index.asp">Garant&iacute;as Estatales</a></li><li><a href="/empresas/pagos_en_linea/index.asp">Pagos por Internet</a></li><li><a href="/empresas/clientes-internacionales/index.asp">Clientes Internacionales <span class="icon-flecha-abajo"></span></a>
				<ul class="submenuDesplegable">
					<li><a href="/empresas/clientes-internacionales/international-desk/index.asp">International Desk</a></li>
					<li><a href="/empresas/clientes-internacionales/santander-passport/index.asp">Santander passport</a></li>
					<li><a href="/empresas/clientes-internacionales/santander-trade/index.asp">Santander Trade</a></li>
					<li><a href="/empresas/clientes-internacionales/santander-trade-network/index.asp">Santander Trade Network</a></li>
				</ul></li>
			</ul>
			<ul id="submenuPrivate" class="displayNone">
				<li class="menuVacio"></li>
			</ul>
			<ul id="submenuUniversidades" class="displayNone">
				<li class="menuVacio"></li>
			</ul>
		</div>
	</div>
	<div id="menuSantanderMobile" class="menuSantanderMobile">
		<div id="menuSantander" class="botonHamburguesa">
			<span class="linea"></span>
		</div>
	</div>
	<div id="desplegableMenu" class="menuDesplegable menuOculto scrollNew">
		<form name="autent" method="post" action="<%=secWebName%>/transa/cruce.asp" target="_top" onSubmit="return submit_envio(document.autent.d_rut.value, document.autent.d_pin.value, 0,1);" id="rut-banner-mobile">
			<div class="rutyclaveinterior">
			    <div class="cajaInput inputWrap">
			    	<input class="input"  type="text" name="d_rut" maxlength="12" size="12" onchange="document.autent.botonenvio.disabled=false;" autocomplete="off" >
					<span class="barra"></span>
				    <label class="label" for="d_rut">RUT</label>
			    </div>
			    <div class="cajaInput">
			    	<input class="input" type="password" name="d_pin" maxlength="8" size="4"value="" onchange="document.autent.botonenvio.disabled=false;" autocomplete="off">
					<span class="barra"></span>
				    <label class="label" for="d_pin" >Clave</label>
			    </div>
			    <div class="recuperar">
			    	<a class="botonTextoLink" href="/transa/productos/clavesantander/ingresorut.asp">&iquest;No tienes tu clave?</a>
			    </div>
			</div>
			<a href="javascript:void(0);" id="loginMenu" class="ingresaraBanco"><span class="icon-candado iconoIzquierda"></span>Ingresar a mi banco</a>
			<input type="submit" name="botonenvio" id="botonenvio" class="btn-submit ingresaraBanco logInOcultar" value="Ingresar"/>
			<input type="hidden" name="hrut">
			<input type="hidden" name="pass">
			<input type="hidden" id="optsgd" name="optsgd" maxlength="3" size="3" value="ATN"/>
			<%'HOMEBANKING %>
			<input type="hidden" name="rut" />
			<input type="hidden" name="IDLOGIN" value="BancoSantander" />
			<input type="hidden" name="tipo" />
			<input type="hidden" name="pin" maxlength="8" size="8" />
			<input type="hidden" name="usateclado" value="no" />
			<input type="hidden" name="dondeentro" value="home" />
			<input type="hidden" name="rslAlto" value="0" />
			<input type="hidden" name="rslAncho" value="0" />
		</form>
		<form name="passemp" id="form-empresas-mobile" method="post" action="https://www.santandersantiago.cl/transa/cruce.asp" target="_top">
			<% 'OFFICEBANKING %>
			 <input type="hidden" name="hrut">
			<input type="hidden" name="pass">
			<input type="hidden" id="optsgd" name="optsgd" maxlength="3" size="3" value="ATN"/>
			<input type="hidden" name="IDLOGIN" value="BancoSantander">
			<input type="hidden" name="pin" value="" maxlength="8" >
			<input type="hidden" name="rut" value="">
			<input type="hidden" name="usateclado" value="no">
			<input type="hidden" name="rslAlto" value="0">
			<input type="hidden" name="rslAncho" value="0">
			<input type="hidden" name="dondeentro" value="home">
		</form>
		<ul class="listadoMenu">
			<li id="mobilePersonas">
				<a class="liAnimado" href="javascript:void(0);">Personas</a>
				<span class="icon-flecha-abajo"></span>
				<ul id="asd" class="displayNone">
					<li><a href="/index.asp">Inicio</a></li>
					<li><a href="/nuestro_banco/index.asp">Nuestro Banco</a></li>
					<li><a href="/nuestros_productos/index.asp">Nuestros Productos</a></li>
					<li><a href="/cuenta-corriente.asp">Hazte Cliente</a></li>
					<li><a href="/simuladores/personas/credito_personal/simulador.asp">Cr&eacute;dito Personal</a></li>
					<li><a href="/tarjetas/index.asp">Tarjetas</a></li>
					<li><a href="/seguros/index.asp">Seguros</a></li>
					<li><a href="/inversiones/index.asp">Inversiones</a></li>
					<li><a href="/hipotecario/index.asp">Mundo Hipotecario</a></li>
				</ul>
			</li> 
			<li id="mobileSelect">
				<a class="liAnimado" href="javascript:void(0);">Select</a>
				<span class="icon-flecha-abajo"></span>
				<ul class="displayNone">
					<li><a href="/select/index.asp">Inicio</a></li>
					<li><a href="/select/select.asp">Santander Select</a></li>
					<li><a href="/select/atencion.asp">Atenci&oacute;n</a></li>
					<li><a href="/select/productos.asp">Productos</a></li>
					<li><a href="/select/inversion.asp">Inversiones</a></li>
					<li><a href="/select/financiamiento.asp">Financiamiento</a></li>
					<li><a href="/select/proteccion.asp">Seguros</a></li>
					<li><a href="/select/santander_experience.asp">Select Experience</a></li>
					<li><a href="/select/cliente_select.asp">S&eacute; Cliente Select</a></li>
					<li><a href="/select/global-value/index.asp">Global Value</a></li>
				</ul>
			</li> 
			<li id="mobileAdvance"><a class="liAnimado" href="javascript:void(0);">Pymes</a>
				<span class="icon-flecha-abajo"></span>
				<ul class="displayNone">
					<li><a href="/advance/index.asp">Inicio</a></li>
					<li><a href="/advance/financiamiento.asp">Financiamiento</a></li>
					<li><a href="/advance/tarjetas/tarjeta-advance-debito.asp">Tarjetas</a></li>
					<li><a href="/advance/seguros.asp">Seguros</a></li>
					<li><a href="/advance/inversiones-y-derivados.asp">Inversiones</a></li>
					<li><a href="/advance/productos-comex.asp">Comercio Exterior</a></li>
					<li><a href="/advance/hazte-cliente/index.asp">Hazte Cliente</a></li>
				</ul>
			</li> 
			<li id="mobileEmpresas"><a class="liAnimado" href="javascript:void(0);">Empresas</a>
				<span class="icon-flecha-abajo"></span>
				<ul class="displayNone">
					<li><a href="/empresas/index.asp">Inicio</a></li>
					<li><a href="/empresas/nuestros_productos/index.asp">Nuestros Productos</a></li>
					<li><a href="/empresas/nuestros_productos/financiamiento.asp">Solicita tu Cr&eacute;dito</a></li>
					<li><a href="/tarjetas/empresas/nuestras-tarjetas/index.asp">Tarjetas</a></li>
					<li><a href="/empresas/garantias_estatales/index.asp">Garant&iacute;as Estatales</a></li>
					<li><a href="/empresas/pagos_en_linea/index.asp">Pagos por Internet</a></li>
					<li><a href="/empresas/clientes-internacionales/index.asp">Clientes Internacionales</a></li>
					<li><a href="/empresas/hazte_cliente/plan-empresas.asp">Hazte Cliente</a></li>
				</ul>
			</li> 
			<li id="mobilePrivate"><a class="liAnimado" href="http://www.santanderpb.cl">Private Banking</a><span class="icon-flecha-derecha"></span></li>
			<li id="mobileGcb"><a class="liAnimado" href="/GCB/index.asp" target="_blank" >GCB</a><span class="icon-flecha-derecha"></span></li>
			<li id="mobileUniversidades"><a class="liAnimado" href="/universidades/index.asp">Universidades</a><span class="icon-flecha-derecha"></span></li>
		</ul>
		<div class="footerMobile">
			<a class="salaPrensa" href="https://saladecomunicacion.santander.cl/" target="_blank"><span class="icon-audio"></span> Sala de Prensa</a>
			<a class="voxMobile" href=""><span class="icon-superlinea"></span> Ll&aacute;manos al 600 600 3000</a>
			<a class="servicioCliente" href=""><span class="icon-empleados-santander"></span> Servicio al Cliente</a>
			<a class="chatOnline" href=""><span class="icon-app"></span>Chat Online</a>
			<a class="preguntasFrecuentes" href=""><span class="icon-favoritos"></span>Preguntas Frecuentes</a>
		</div>
	</div>
</div>
<div class="menuFantasma"></div>
<div class="bgFixedMenuSantander bgFixedMenuOculto"></div>