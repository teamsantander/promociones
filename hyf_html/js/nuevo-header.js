$( document ).ready(function() {
    var element = document.querySelectorAll('li a');
    element[5].setAttribute('href','/GCB');
    element[5].setAttribute('target','');

    /*$(".menuIE7 .secundarioIE7 .contenedorBotones ul li").mouseover(function() {
        $(".menuIE7 .secundarioIE7 .contenedorBotones ul li .desplegableIE7").show();
    });*/

    hrefCalugas();

    var url_position = 4;  
                       //1 -> Santander.cl  
                       //2 -> 73   
                       //3 -> 51(ambiente de desarrollo banco)

    if(url_position==1){
            var url_dinamica = "/tarjetas/personas/";
            //console.log(url_dinamica);
    }else if (url_position==2){
            var url_dinamica= "/tarjetas/tarjetas-santander/";  
            //console.log(url_dinamica);
    }else{
            var url_dinamica = "/tarjetas/";
            //console.log(url_dinamica);
    }

    localStorage.setItem("url_dinamica", url_dinamica);
    var url_dim = localStorage.getItem("url_dinamica");

    let url_nuestras = window.location.href.split('/');
    let indexUrlNuestra =  url_nuestras.length - 2;
    var url_controllerNuestras = url_nuestras[indexUrlNuestra];

    let url_tarjetas = window.location.href.split('/');
    let indexUrl =  url_tarjetas.length - 3;
    var url_controller = url_tarjetas[indexUrl];


    if(window.location.pathname === url_dim + 'nuestras-tarjetas/' || window.location.pathname === url_dim + 'nuestras-tarjetas/index.asp' || url_controllerNuestras === 'nuestras-tarjetas' ){
        css('encuentra-tu-tarjeta/assets/css/main.css');
        script('nuestras-tarjetas/assets/js/all.js');
        script('nuestras-tarjetas/assets/js/titulos.js')
    } else if (window.location.pathname === url_dim + 'campanas/' || window.location.pathname === url_dim + 'campanas/index.asp'){
        script('campanas/js/funciones_santander.js');
    } else if (window.location.pathname === url_dim + 'encuentra-tu-tarjeta/' || window.location.pathname === url_dim + 'encuentra-tu-tarjeta/index.asp'){
        css('encuentra-tu-tarjeta/assets/css/main.css');
        script('encuentra-tu-tarjeta/assets/js/range.js');
        script('encuentra-tu-tarjeta/assets/js/filter.js');
    } else if (window.location.pathname === url_dim || window.location.pathname === url_dim + 'index.asp'){
        css('assets/css/jquery.bxslider.css');
        css('assets/css/main.css');
        css('assets/css/home.css');
        css('assets/css/contenedor-slider.css');
        script('assets/js/main.js');
    } else if(url_controller === "tarjetas"){
        css('nuestras-tarjetas/assets/css/estilos.css');
        script('nuestras-tarjetas/assets/js/drop.js');
        script('nuestras-tarjetas/assets/js/function.js');
        script('nuestras-tarjetas/assets/js/acordes.js');
        script('nuestras-tarjetas/controllers.js');
    } else if(window.location.pathname === url_dim + 'preguntas-frecuentes/index.asp' || window.location.pathname === url_dim + 'preguntas-frecuentes/'){
        css('nuestras-tarjetas/assets/css/estilos.css');
        css('preguntas-frecuentes/assets/css/estilos.css');
        script('preguntas-frecuentes/assets/js/funciones.js');

    }


    $( ".menuIE7 .secundarioIE7 .contenedorBotones ul li" ).hover(
        function() {
            $( this ).find(".desplegableIE7").show();
        }, function() {
            $( this ).find(".desplegableIE7").hide();
        }
    );
    $("#desplegableKit ul li a").click(function() {
        $("#menuKit .linea").toggleClass("equis");
        $("#desplegableKit").toggleClass("menuOculto");
        $(".bgFixedMenu").toggleClass("bgFixedMenuOculto");
    });
    $("#menuSantander").click(function() {
        $("#menuSantander .linea").toggleClass("equis");
        $("#desplegableMenu").toggleClass("menuOculto");
        $(".bgFixedMenuSantander").toggleClass("bgFixedMenuOculto");
        $(".rutyclaveinterior").slideUp();
        $("#loginMenu").removeClass("logInOcultar");
        $("#loginEnviar").addClass("logInOcultar");
        $("#desplegableMenu ul ul").slideUp();
        $(".liAnimado").toggleClass("animarMenu");
		$("#loginMenu").removeClass("logInOcultar");
		$("#botonenvio").addClass("logInOcultar");

        $("#desplegableMenu > ul > li:first-child").find("span").addClass("icon-flecha-arriba");
        $("#desplegableMenu > ul > li#mobileUniversidades").find("span").removeClass("icon-flecha-arriba").addClass("icon-flecha-derecha");
        $("#desplegableMenu > ul > li:first-child").find("ul").show();
    });
	$(".ayuda").click(function() {
		$(".submenuAyuda").toggleClass("displayNone");
        $(".ayuda").toggleClass("activoAyuda");
	});
    $("#loginMenu").click(function(){
        $(".rutyclaveinterior").slideDown();
        $("#botonenvio").removeClass("logInOcultar");
        $("#loginMenu").addClass("logInOcultar");
    });
    $("#desplegableMenu").scroll(function(){
		var w_rut = $(window).width();
		if(w_rut < 1060) {
			$(".rutyclaveinterior").slideUp();
			$("#loginMenu").removeClass("logInOcultar");
			$("#botonenvio").addClass("logInOcultar");
		}
    });
	fixedrutyclave();


    if(window.attachEvent){
        document.attachEvent("resize",fixedrutyclave);
    }else{
        document.addEventListener("resize",fixedrutyclave);
    }

    var urlActual = String(window.location);

    var esChat = urlActual.indexOf("chat-online");
    var esServ = urlActual.indexOf("servicio_al_cliente");
    var esCred = urlActual.indexOf("promociones/credito/");
    var esRecomenCtaCte = urlActual.indexOf("recomendaciones/recomienda-cuenta-corriente");
        if(esChat > 0){
        $("#rut-banner-mobile").remove();
    }
        if(esServ > 0){
        $("#rut-banner-mobile").remove();
    }
        if(esRecomenCtaCte > 0){
        $("#rut-banner-mobile").remove();
    }
    if(esCred > 0){
        $("#rut-banner-mobile").remove();
    }
    //Agregamos el itilimun (como se escriba) dinamico
    
    //Paso dos preguntamos si se encuentra la variable si no esta la agregamos
    if(  typeof utag_data == 'undefined' || utag_data === 'undefined' ){ 
      var script_var = document.createElement("script");
      script_var.innerHTML = " var utag_data = { } ";
      document.head.appendChild(script_var);

      var function_var = document.createElement("script");
      function_var.innerHTML = '(function(a,b,c,d){ a="//tags.tiqcdn.com/utag/santander/ch-online-banking/prod/utag.js"; b=document;c="script"; d=b.createElement(c);d.src=a;d.type="text/java"+c;d.async=true; a=b.getElementsByTagName(c)[0]; a.parentNode.insertBefore(d,a); })();';
      document.head.appendChild(function_var);

    }else{

    // alert('tiene');
        
    }



    /* 
        Generador de id dinamico con milisegundos
    */

        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear()+(month<10 ? '0' : '') +month+(day<10 ? '0' : '')+day;
        var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        
        var mi_id = output+time;
        $('#id_gdconv').val(mi_id);

        $("form").each(function(){
        var formulario_is = $(this).attr("data-selector");

        if(formulario_is=="form_id_var"){  //Si solamente queremos agregale un id unico usamos esto en el data-salector="form_id_var"
            $(this).append("<input type='hidden' name='id_gdconv' id='id_gdconv' value=''>");
        }

        if(formulario_is=="form_utm_var"){ //Si solamente queremos agregale los utm  usamos esto en el data-salector="form_utm_var"
            var utm_source = getUrlParameter('utm_source'); //Capturamos los datos pasado por paramatros
            if(utm_source==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_source = "";
            }
            var utm_medium = getUrlParameter('utm_medium'); //Capturamos los datos pasado por paramatros
            if(utm_medium==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_medium = "";
            }
            var utm_campaign = getUrlParameter('utm_campaign'); //Capturamos los datos pasado por paramatros
             if(utm_campaign==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_campaign="";
             }
            var utm_term = getUrlParameter('utm_term'); //Capturamos los datos pasado por paramatros
            if(utm_term==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_term="";
            }
            var utm_content = getUrlParameter('utm_content'); //Capturamos los datos pasado por paramatros
            if(utm_content==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_content="";
            }
            $(this).append('<input type="hidden" name="utm_source" id="utm_source" value="'+utm_source+'" />'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_medium" id="utm_medium" value="'+utm_medium+'"/>');  //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_campaign" id="utm_campaign" value="'+utm_campaign+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_term" id="utm_term" value="'+utm_term+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_content" id="utm_content" value="'+utm_content+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
        

        }   

        if(formulario_is=="form_mixto"){ //Si queremos agregarle ambos tanto el id como el utm en el data-selector="form_mixto"

            $(this).append("<input type='hidden' name='id_gdconv' id='id_gdconv' value=''>");

            var utm_source = getUrlParameter('utm_source'); //Capturamos los datos pasado por paramatros
            if(utm_source==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_source = "";
            }
            var utm_medium = getUrlParameter('utm_medium'); //Capturamos los datos pasado por paramatros
            if(utm_medium==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_medium = "";
            }
            var utm_campaign = getUrlParameter('utm_campaign'); //Capturamos los datos pasado por paramatros
             if(utm_campaign==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_campaign="";
             }
            var utm_term = getUrlParameter('utm_term'); //Capturamos los datos pasado por paramatros
            if(utm_term==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_term="";
            }
            var utm_content = getUrlParameter('utm_content'); //Capturamos los datos pasado por paramatros
            if(utm_content==null){ //Preguntamos si es distinto si no deja la variable vacia
                utm_content="";
            }
            $(this).append('<input type="hidden" name="utm_source" id="utm_source" value="'+utm_source+'" />'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_medium" id="utm_medium" value="'+utm_medium+'"/>');  //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_campaign" id="utm_campaign" value="'+utm_campaign+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_term" id="utm_term" value="'+utm_term+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
            $(this).append('<input type="hidden" name="utm_content" id="utm_content" value="'+utm_content+'"/>'); //insertamos los input al formulario y luego le agregamos las variables
        }

    });

    //Validamos formularios 

    if($('#wrapper').find('form').length){  //Preguntamos si el formulario esta , si lo esta removemos el form del header

            $('#rut-banner-mobile').remove();
            
    }else{ // Del caso contrario podemos crear otra accion
        
        //Por si necesitamos hacer otra validacion
    }



});

function fixedrutyclave(){
	var w_rut = $(".menuSantander").width();
	if(w_rut < 1060) {
        //$("#rut_lave, #rut-clave").attr("style", "display:none");
        $("#rut_lave").find("form[name='autent']").attr('name', 'autent_');
        
        $("#cont_rut_clave").attr("style", "display:none");
        $("#cont_rut_clave").find("form[name='autent']").attr('name', 'autent_');
        
        $("#rut-banner").attr('name', 'autent_');
        $("#rut-banner-mobile").attr('name', 'autent');

        $("#form-empresas-desktop").attr('name', 'passemp_');
        $("#rut_lave").find("form[name='passemp']").attr('name', 'passemp_');
        $("#form-empresas-mobile").attr('name', 'passemp');
    }else{
        $("#rut_lave, #rut-clave").removeAttr("style");
        $("#cont_rut_clave").removeAttr("style");
        $("#rut-banner").attr('name', 'autent');
        $("#rut_lave").find("form[name='autent']").attr('name', 'autent');
        $("#cont_rut_clave").find("form[name='autent']").attr('name', 'autent');
        $("#rut-banner-mobile").attr('name', 'autent_');
        $("#form-empresas-desktop").attr('name', 'passemp');
        $("#rut_lave").find("form[name='passemp_']").attr('name', 'passemp');
        $("#form-empresas-mobile").attr('name', 'passemp_');
    }
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


// Function location url calugas.

function hrefCalugas(){
    setTimeout(function(){
        var href = document.querySelectorAll('a[data-href]');

        href.forEach(function(i){
            var title = i.getAttribute('title');
            var link = i.getAttribute('data-href');
            i.removeAttribute('href');
            i.setAttribute('onclick', "link('"+link+"','"+title+"')");
        });
    },1000)
}

function link(url,title){
    var o = url;
    var i = o.split('/');
    if(i[0] === 'http:' || i[0] === 'https:'){
        window.open(url);
        setTimeout(function(){
            ga('send','event', 'Personas', title, 'Tab');
        },2000);
        return false;
    }
    else{
        location.href = url;
        setTimeout(function(){
            ga('send','event', 'Personas', title, 'Tab');
        },2000);
        return false;
    }
    return false;
}

function script(ruta){
    let script = document.createElement('script');
    script.setAttribute('src', localStorage.getItem("url_dinamica") + ruta);
    script.setAttribute('type','text/javascript');
    document.body.appendChild(script);
}

function css(ruta){
    let link = document.createElement('link');
    link.setAttribute('href', localStorage.getItem("url_dinamica") + ruta);
    link.setAttribute('type','text/css');
    link.setAttribute('rel','stylesheet');
    document.head.appendChild(link);
}

